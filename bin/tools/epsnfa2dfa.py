#!/usr/bin/env python
# -*- coding: utf-8 -*-


def scanNFA(infile):
    NFA = []
    maxstate = 1
    for line in infile:
        # parse transition
        # format: (source, character, destination)
        transition = line.split(',')
        s = int(transition[0])
        char = transition[1]
        d = int(transition[2].strip('\n'))
        maxstate = max(maxstate, s + 1, d + 1)
        if maxstate > len(NFA):
            for i in range(maxstate - len(NFA)):
                NFA.append(dict())

        if NFA[s] == {}:
            NFA[s] = {}
            NFA[s][char] = [d]
        elif char not in NFA[s]:
            NFA[s][char] = [d]
        else:
            NFA[s][char].append(d)
    return NFA


def epsclose(NFA):
    Eclosure = []
    for state in range(len(NFA)):
        Eclosure.append(set([state]))  # initialize

    changed = True
    while changed is True:
        changed = False
        for state in range(len(NFA)):
            if 'FF' in NFA[state]:
                for dest in NFA[state]['FF']:
                    if not Eclosure[dest].issubset(Eclosure[state]):
                        Eclosure[state] = Eclosure[state].union(Eclosure[dest])
                        changed = True
    return Eclosure


def nfa2dfa(NFA, EpsClosure):
    # DFA constructor by subset construction

    DFA = []

    alphalist = [set(NFA[i].keys()) for i in range(len(NFA))]
    ALPHABET = filter(lambda x: x != 'FF',
                      reduce(lambda x, y: x | y, alphalist))

    # "statedict" is a table that stores mapping
    # from stateset to DFA state number
    statedict = dict()

    newstate_queue = []

    # start subset construction from initial state of NFA
    initialstate = EpsClosure[0]
    state_num = _set2state(statedict, initialstate)
    DFA.append(dict())
    for char in ALPHABET:
        dest_state = set()
        for state in initialstate:
            if char in NFA[state]:
                candidate_state = set(NFA[state][char])
                for candidate in candidate_state:
                    dest_state |= EpsClosure[candidate]
            else:
                continue
        DFA[state_num][char] = dest_state
        if not _isExists(statedict, DFA[state_num][char]):
            next_id = _set2state(statedict, DFA[state_num][char])
            nextset = DFA[state_num][char]
            newstate_queue.append((next_id, nextset))
            DFA.append(dict())

    # constructing phase
    while len(newstate_queue) != 0:
        (state_num, stateset) = newstate_queue.pop(0)
        for char in ALPHABET:
            dest_state = set()
            for state in stateset:
                if char in NFA[state]:
                    candidate_state = set(NFA[state][char])
                    for candidate in candidate_state:
                        dest_state |= EpsClosure[candidate]
                else:
                    continue
            DFA[state_num][char] = dest_state
            if not _isExists(statedict, dest_state):
                next_id = _set2state(statedict, dest_state)
                nextset = DFA[state_num][char]
                newstate_queue.append((next_id, nextset))
                DFA.append(dict())
        #printFA(DFA)
    return DFA


def _isExists(statedict, stateset):
    # return True if DFA has a state corresponding to input stateset
    state_str = ''.join(map(str, stateset))
    if state_str in statedict:
        return True
    else:
        return False

    
def _set2state(statedict, stateset):
    # This method calculate state number from stateset
    state_str = ''.join(map(str, stateset))
    if _isExists(statedict, stateset):
        return statedict[state_str]
    else:
        new_state = len(statedict)
        statedict[state_str] = new_state
        return new_state


def printFA(FA):
    index = 0
    print
    for elem in FA:
        print index, elem
        index += 1


def outputDFA(filename, DFA):
    outfile = open(filename, 'w')
    for state in range(len(DFA)):
        for char in DFA[state].keys():
            for dest in DFA[state][char]:
                outfile.write('%d,%s,%d\n' % (state, char, dest))


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('infile', type=argparse.FileType('r'),
                        help='Input file name.'
                        )
    parser.add_argument('outfile', type=str,
                        help='Output file name.'
                        )
    parser.add_argument('--version', action='version', version='%(prog)s 1.0')
    args = parser.parse_args()

    NFA = scanNFA(args.infile)

    Eps = epsclose(NFA)
    #print Eps
    DFA = nfa2dfa(NFA, Eps)
    outputDFA(args.outfile, DFA)
