package ONE1eps;

import java.io.IOException;
import java.math.BigInteger;

import jp.ac.tsukuba.mdl.hiroki.cryptgraphi.Paillier;
import Output.Timer;
import base.AHMultiplier;
import base.ConnectionManager;

public class AHepsMultiplier extends AHMultiplier{

	public AHepsMultiplier(Paillier fPaillier, int fAutomatonLen,
			ConnectionManager fConnection) {
		super(fPaillier, fAutomatonLen, fConnection);
	}
	
	public void epsMulti(int[] epsInfo) throws IOException, ClassNotFoundException{
		ConnectionManager connection = super.fConnection;

		BigInteger[] encPrimeS = (BigInteger[])connection.readData();
		BigInteger[] primeS = new BigInteger[super.fAutomatonLen];
		
		Timer ahTimer = new Timer(1);
		ahTimer.start();
		for(int i = 0; i < encPrimeS.length; i++){
			primeS[i] = super.fPaillier.dec(encPrimeS[i]);
		}
		BigInteger[] barS = new BigInteger[super.fAutomatonLen];
		BigInteger[] encBarS = new BigInteger[super.fAutomatonLen];
		BigInteger[] encStarS = new BigInteger[super.fAutomatonLen];

		for(int i = 0; i < encPrimeS.length; i++){
			if(epsInfo[i] == i){
				barS[i] = primeS[i].multiply(primeS[i]);
				encStarS[i] = encPrimeS[i];
			}else{
				barS[i] = primeS[i].multiply(primeS[epsInfo[i]]);
				encStarS[i] = encPrimeS[epsInfo[i]];
			}
			encBarS[i] = super.fPaillier.enc(barS[i]);
		}
		ahTimer.stop();
		
		connection.writeData(encBarS);
		connection.writeData(encStarS);
		connection.writeData(ahTimer.getfRunningTime());
		super.multi();	
	}
}
