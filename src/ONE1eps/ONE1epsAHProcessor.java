package ONE1eps;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;

import base.ConnectionManager;

import com.sun.org.apache.bcel.internal.generic.FNEG;


import ONEcomp.LoopMask;
import ONEcomp.NFAreader;
import ONEcomp.ONEcompAHProcessor;

public class ONE1epsAHProcessor extends ONEcompAHProcessor{
	AHepsMultiplier fepsMultiplier;
	int[] fEpsInfo;
	
	public ONE1epsAHProcessor(int fAutomatonSize, int keylen,
			ConnectionManager connection) {
		super(fAutomatonSize, keylen, connection);
		fepsMultiplier = new AHepsMultiplier(super.fPaillier, super.fAutomatonSize, connection);
	}
	
	@Override
	public BigInteger[][] makeEncMask(NFAreader nfa) {
		return super.makeEncMask(nfa);
	}

	@Override
	public BigInteger[][] makeEncLoopMask(NFAreader nfa) {
		return super.makeEncLoopMask(nfa);
	}

	public BigInteger[][] makeEncEpsMatrix(NFAreader nfa){
		BigInteger[][] encEpsMatrix = new BigInteger[super.fAutomatonSize][super.fAutomatonSize];
		for(int i = 0; i < super.fAutomatonSize; i++){
			for(int j = 0; j < super.fAutomatonSize; j++){
				encEpsMatrix[i][j] = super.fPaillier.enc(nfa.getfEpsInfo()[i][j]);
			}
		}
		return encEpsMatrix;
	}
	
	public void makeEpsInfos(NFAreader nfa){
		fEpsInfo = new int[super.fAutomatonSize];
		for(int i = 0; i < super.fAutomatonSize; i++){
			fEpsInfo[i] = i;
			for(int j = 0; j < super.fAutomatonSize; j++){
				if(nfa.getfEpsInfo()[i][j] == 1){
					fEpsInfo[i] = j;
				}
			}
		}
	}

	@Override
	public void loopUpdate() throws IOException, ClassNotFoundException {
		super.fMultiplier.multi();
	}
	
	@Override
	public void epsUpdate() throws IOException, ClassNotFoundException {
		fepsMultiplier.epsMulti(fEpsInfo);
	}
}
