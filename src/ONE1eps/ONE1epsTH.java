package ONE1eps;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;



import ONEcomp.ONEcompTH;
import ONEcomp.ONEcompTHProcessor;
import ONEcomp.State;
import Output.OutputData;

public class ONE1epsTH extends ONEcompTH{
	ONE1epsTHProcessor f1epsProcessor;
	
	public ONE1epsTH(String fText, int fPort, OutputData fOutput) throws IOException {
		super(fText, fPort, fOutput);
	}

	@Override
	public void setupTH(int sigmaSize) throws IOException,
			ClassNotFoundException {
		int automatonSize = (Integer)super.fConnection.readData();
		ArrayList<Integer> clientSigma = (ArrayList<Integer>)super.fConnection.readData();
		BigInteger pk = (BigInteger)super.fConnection.readData();
		int msgSpaceSize = (Integer)super.fConnection.readData();
		f1epsProcessor = new ONE1epsTHProcessor(automatonSize, pk, msgSpaceSize, super.fConnection, super.fOutputData);
		
		ArrayList<Integer> mergedSigma = f1epsProcessor.makeSigma(clientSigma, super.fText, sigmaSize);
		f1epsProcessor.setfIDText(f1epsProcessor.makeTextIntoID(super.fText));
		super.fConnection.writeData(mergedSigma);
		
		BigInteger[][] encMask = (BigInteger[][])super.fConnection.readData();
		BigInteger[][] encLoopMask = (BigInteger[][])super.fConnection.readData();
		BigInteger[][] encEpsMatrix = (BigInteger[][])super.fConnection.readData();
		f1epsProcessor.setMasks(encMask, encLoopMask);
		f1epsProcessor.setEpsMatrix(encEpsMatrix);
	
		super.fCurrentState = new State(automatonSize, pk);
		super.fCurrentState.initStates();
	}

	@Override
	public OutputData evaluateAutomaton(int param) throws IOException,
			ClassNotFoundException {
		super.fConnection.writeData(fText.length());
		
		for(int i = 0; i < super.fText.length(); i++){
			State followingState;
			followingState = f1epsProcessor.charTransUpdate(fCurrentState, i);
			followingState.setfStates(f1epsProcessor.loopUpdate(followingState, fCurrentState, i));
			followingState.setfStates(f1epsProcessor.epsUpdate(followingState, i));
			fCurrentState.setfStates(followingState.getfStates());
			f1epsProcessor.getfOutput().endRound(i);
			super.sendResult(followingState);
		}
		f1epsProcessor.getfOutput().makeResult();
		return f1epsProcessor.getfOutput();
	}
	
}
