package ONE1eps;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;

import base.ConnectionManager;

import com.sun.org.apache.bcel.internal.generic.BIPUSH;


import ONEcomp.ONEcompTHProcessor;
import ONEcomp.State;
import Output.OutputData;

public class ONE1epsTHProcessor extends ONEcompTHProcessor{
	THepsMultiplier fepsMultiplier;
	BigInteger[][] fEncEpsMatrix;
	public ONE1epsTHProcessor(int automataSize, BigInteger pk, int msgSpaceSize,
			ConnectionManager connection, OutputData output) {
		super(automataSize, pk, msgSpaceSize, connection, output);
		fepsMultiplier = new THepsMultiplier(super.fPaillier, super.fAutomatonSize, connection, msgSpaceSize);
	}

	public void setMasks(BigInteger[][] encMask, BigInteger[][] encLoopMask) {
		super.fEncMask = encMask;
		super.fEncLoopMask = encLoopMask;
	}
	
	public void setEpsMatrix(BigInteger[][] encEpsMatrix){
		fEncEpsMatrix = encEpsMatrix;
	}

	@Override
	public ArrayList<Integer> makeSigma(ArrayList<Integer> clientSigma,
			String text, int sigmaSize) {
		return super.makeSigma(clientSigma, text, sigmaSize);
	}

	@Override
	public State charTransUpdate(State previousState, int index) {
		return super.charTransUpdate(previousState, index);
	}

	@Override
	public BigInteger[] loopUpdate(State followingState, State previousState,
			int index) throws IOException, ClassNotFoundException {
		return super.loopUpdate(followingState, previousState, index);
	}

	@Override
	public BigInteger[] epsUpdate(State followingState, int index)
			throws IOException, ClassNotFoundException {
		return fepsMultiplier.epsMultiply(followingState.getfStates(), fEncEpsMatrix, fOutput);
	}	

}
