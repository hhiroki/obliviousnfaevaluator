package ONE1eps;

import java.io.IOException;
import java.math.BigInteger;

import com.sun.org.apache.bcel.internal.generic.BIPUSH;

import jp.ac.tsukuba.mdl.hiroki.cryptgraphi.Paillier;
import Output.OutputData;
import Output.Timer;
import base.ConnectionManager;
import base.THMultiplier;

public class THepsMultiplier extends THMultiplier{

	public THepsMultiplier(Paillier fPaillier, int fAutomatonLen,
			ConnectionManager fConnection, int msgBitSize) {
		super(fPaillier, fAutomatonLen, fConnection, msgBitSize);
		// TODO Auto-generated constructor stub
	}
	
	public BigInteger[] epsMultiply(BigInteger[] state, BigInteger[][] encEpsMask, OutputData output) throws IOException, ClassNotFoundException{
		output.getThTimer().start();
		BigInteger[] r = makeRandomNums(super.fRand, fAutomatonLen);
		BigInteger[] primeS = randomize(state, r);
		output.getThTimer().stop();
		
		Timer temp = new Timer(1);
		temp.start();
		super.fConnection.writeData(primeS);
		BigInteger[] barS = (BigInteger[])super.fConnection.readData();
		BigInteger[] starS = (BigInteger[])super.fConnection.readData();
		temp.stop();
		
		long ahCompTime = (Long)fConnection.readData();
		for(int i  = 0; i < primeS.length; i++){
			output.getCommSize().addComm(primeS[i].bitLength());
			output.getCommSize().addComm(barS[i].bitLength());
			output.getCommSize().addComm(starS[i].bitLength());
		}
		output.getAhTimer().setfRunningTime(ahCompTime);
		output.getCommTimer().setfRunningTime(temp.getfRunningTime() - ahCompTime);
		
		output.getThTimer().start();
		BigInteger[] starR = matrixMultiply(encEpsMask, r);
		BigInteger[] nextS = new BigInteger[fAutomatonLen];
		output.getThTimer().stop();
		
		BigInteger[] multiResult = super.multi(starR, primeS, output);
		
		output.getThTimer().start();
		for(int i = 0; i < super.fAutomatonLen; i++){
			nextS[i] = barS[i];
			BigInteger temp1 = super.fPaillier.subtractCipher(starS[i], starR[i]);
			nextS[i] = super.fPaillier.subtractCipher(nextS[i], super.fPaillier.multiplyCipher(temp1, r[i]));
			nextS[i] = super.fPaillier.subtractCipher(nextS[i], multiResult[i]);
		}
		output.getThTimer().stop();
		
		return nextS;
	}
	
	private BigInteger[] matrixMultiply(BigInteger[][] matrix, BigInteger[] vector){
		BigInteger[] ans = new BigInteger[super.fAutomatonLen];
		for(int i = 0; i < matrix.length; i++){
			ans[i] = fPaillier.enc(BigInteger.ZERO);
			for(int j = 0; j < matrix[0].length; j++){
				BigInteger tmp2 = fPaillier.multiplyCipher(matrix[i][j], vector[j]);
				ans[i] = fPaillier.addCipher(ans[i], tmp2);
			}
		}
		return ans;
	}	
}
