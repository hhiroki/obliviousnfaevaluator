package ONEcomp;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;

import jp.ac.tsukuba.mdl.hiroki.cryptgraphi.Paillier;


public class LoopMask {
	int[][] fPlainLoopMask; //[sigmalen][patlen+1]
	BigInteger[][] fEncryptedLoopMask;
	ArrayList<Integer> fSigma;
	
	public LoopMask(ArrayList<Integer> sigma, int fAutomataSize) {
		int sigmalen = sigma.size();
		fPlainLoopMask = new int[sigmalen][fAutomataSize];
		fEncryptedLoopMask = new BigInteger[sigmalen][fAutomataSize];
		fSigma = sigma;
		for(int i = 0; i < fPlainLoopMask.length; i++){
			for(int j = 0;j < fPlainLoopMask[0].length; j++){
				fPlainLoopMask[i][j] = 1;
			}
		}
	}
	
	public BigInteger[][] getfEncryptedLoopMask() {
		return fEncryptedLoopMask;
	}
	
	public int[][] getfPlainLoopMask() {
		return fPlainLoopMask;
	}

	public void makeLoopMask(ArrayList<ArrayList<Integer>> loopInfo){
		for(int i = 0; i < loopInfo.size(); i++){
			if(loopInfo.get(i).isEmpty() == false){
				for(int k = 0; k < fSigma.size(); k++){
					if(loopInfo.get(i).contains(fSigma.get(k))){
						fPlainLoopMask[k][i] = 0;
					}
				}
			}
		}
	}

	
	public void encryptLoopMask(Paillier paillier){
		for(int i = 0; i < fPlainLoopMask.length; i++){
			for(int j = 0; j < fPlainLoopMask[0].length; j++){
				fEncryptedLoopMask[i][j] = paillier.enc(fPlainLoopMask[i][j]);
			}
		}
	}
	
	public void printLoopMask(){
		for(int i = 0; i < fPlainLoopMask.length ;i++){
			System.out.print("LoopMask of " + fSigma.get(i) + ": ");
			for(int j = 0; j < fPlainLoopMask[0].length; j++){
				System.out.print(fPlainLoopMask[i][j] + " ");
			}
			System.out.println();
		}
	}
}
