package ONEcomp;

import java.math.BigInteger;
import java.util.ArrayList;

import jp.ac.tsukuba.mdl.hiroki.cryptgraphi.Paillier;

public class Mask {
	int[][] fPlainM;
	BigInteger[][] fEncM;
	BigInteger fPK;
	Paillier fPaillier;
	
	//constructor
	public Mask(BigInteger pk) {
		super();
		fPK = pk;
		fPaillier = new Paillier(fPK);
	}
	
	//getter & setter
	public int[][] getfPlainM() {
		return fPlainM;
	}

	public void setfPlainM(int[][] fPlainM) {
		this.fPlainM = fPlainM;
	}

	
	public BigInteger[][] getfEncM() {
		return fEncM;
	}

	public void setfEncM(BigInteger[][] fEncM) {
		this.fEncM = fEncM;
	}

	/**
	 * maskbitを生成.この結果をMaskクラスのfPlainMに代入.
	 * 
	 */
	public int[][] makeMask(ArrayList<Integer> sigma, ArrayList<String> pattern, int automatonSize){
		int[][] newMask = new int[sigma.size()][automatonSize - 1];
				
		for(int i = 0; i < sigma.size(); i++){
			for(int j = 0; j < automatonSize - 1; j++){
					newMask[i][j] = 1;
			}
		}

		for(String str: pattern){
			String[] pat = str.split(",");
			int from = Integer.parseInt(pat[0]);
			int to = Integer.parseInt(pat[2]);
			int value = Integer.parseInt(pat[1]);
			
			if(from + 1 != to){
				System.out.println("Wrong regexp: the Automaton is not applicable form.");
				System.exit(1);
			}
			for(int i = 0; i < sigma.size(); i++){
				if(value == sigma.get(i)){
					newMask[i][from] = 0;
				}
			}
		}
		return newMask;
	}
	
	/**
	 * maskbitを暗号化.この結果をMaskクラスのfencMに代入
	 * @return
	 */
	public BigInteger[][] encryptMask(){
		BigInteger[][] encryptedMask = new BigInteger[fPlainM.length][fPlainM[0].length];
		for(int i = 0; i < fPlainM.length; i++){
			for(int j = 0;  j < fPlainM[0].length; j++){
				encryptedMask[i][j] = fPaillier.enc(fPlainM[i][j]);
			}
		}
		return encryptedMask;
	}

	public void print(ArrayList<Integer> sigma){
		for(int i = 0; i < fPlainM.length; i++){
			System.out.print("Mask of " + sigma.get(i) + ": ");
			for(int j = 0; j < fPlainM[i].length; j++){
				System.out.print(fPlainM[i][j] + " ");
				if(j == fPlainM[i].length - 1){
					System.out.println();
				}
			}
		}
	}
}
