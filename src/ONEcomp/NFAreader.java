package ONEcomp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import org.omg.PortableServer.IdUniquenessPolicy;

public class NFAreader {
	File fFile;
	ArrayList<String> fPattern;
	ArrayList<ArrayList<Integer>> fLoopInfo;
	int[][] fEpsInfo;
	ArrayList<Integer> fSigma;
	int fAutomatonLen;
	public NFAreader(File fFile) {
		super();
		this.fFile = fFile;
		fPattern = new ArrayList<String>();
		fSigma = new ArrayList<Integer>();
	}
		
	public ArrayList<String> getfPattern() {
		return fPattern;
	}

	public int getfAutomatonLen() {
		return fAutomatonLen;
	}


	public ArrayList<ArrayList<Integer>> getfLoopInfo() {
		return fLoopInfo;
	}


	public int[][] getfEpsInfo() {
		return fEpsInfo;
	}


	public ArrayList<Integer> getfSigma() {
		return fSigma;
	}


	public void readNFA()throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(fFile));
		String line;
		ArrayList<String[]> nfaList = new ArrayList<String[]>();
		while((line = reader.readLine() ) != null){
			String[] trans = line.split(",");
			nfaList.add(trans);
		}
		
		//decide pattern length
		int automatonLen = findMaxStateNum(nfaList) + 1;
		fAutomatonLen = automatonLen;
		fLoopInfo = new ArrayList<ArrayList<Integer>>();
		fEpsInfo = new int[automatonLen][automatonLen];
		for(int i = 0 ; i < automatonLen;i++){
			fLoopInfo.add(new ArrayList<Integer>());
			for(int j = 0; j < automatonLen; j++){
				fEpsInfo[i][j] = 0;
			}
		}
		
		//make data and sigma
		for(String[] str: nfaList){
			int from = Integer.parseInt(str[0]);
			int value = hexToInteger(str[1]);
			int to = Integer.parseInt(str[2]);
			
			if(value == hexToInteger("FF")){
				fEpsInfo[to][from] = 1; 
			}else{
				if(fSigma.contains(value) != true){
					fSigma.add(value);
				}
				
				if(from == to){
					(fLoopInfo.get(from)).add(value);
				}else{
					fPattern.add(from + "," + value + "," + to);
				}
			}
		}
		
	}
	
	private int hexToInteger(String hex){
		return Integer.parseInt(hex, 16);
	}
	
	private int findMaxStateNum(ArrayList<String[]> nfaList){
		int max = -1;
		for(String[] str : nfaList){
			int fromNum = Integer.parseInt(str[0]);
			int toNum = Integer.parseInt(str[2]);
			if(max < toNum){
				max = toNum;
			}
			if(max < fromNum){
				max = fromNum;
			}
		}
		return max;
	}
}
