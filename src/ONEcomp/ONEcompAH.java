package ONEcomp;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.util.ArrayList;

import jp.ac.tsukuba.mdl.hiroki.cryptgraphi.Key;
import jp.ac.tsukuba.mdl.hiroki.cryptgraphi.Paillier;

import Output.OutputData;
import base.AutomatonHolderEvaluable;
import base.ConnectionManager;
import base.TextHolderEvaluable;

public class ONEcompAH implements AutomatonHolderEvaluable{
	protected NFAreader fNFA; 
	ONEcompAHProcessor fCompProcessor;
	protected int fKeylen;
	protected ConnectionManager fConnection;

	
	public ONEcompAH(int fKeylen, int fPort, InetAddress fIp)
			throws IOException {
		this.fKeylen = fKeylen;
		fConnection = new ConnectionManager(fPort, fIp);
	}
		
	
	@Override
	public void setupAH(File nfaFile) throws IOException, ClassNotFoundException {
		fNFA = new NFAreader(nfaFile);
		fNFA.readNFA();
		fCompProcessor = new ONEcompAHProcessor(fNFA.getfAutomatonLen(), fKeylen, fConnection);
		
		fConnection.writeData(fNFA.getfAutomatonLen());
		fConnection.writeData(fNFA.getfSigma());
		fConnection.writeData(fCompProcessor.fKey.getN());
		fConnection.writeData(fKeylen*2); //encrypted message space.
		
		fCompProcessor.setfSigma((ArrayList<Integer>)fConnection.readData());
		fConnection.writeData(fCompProcessor.makeEncMask(fNFA));
		fConnection.writeData(fCompProcessor.makeEncLoopMask(fNFA));
		fConnection.writeData(fCompProcessor.makeEncEpsMask(fNFA));
	}
	
	@Override
	public void evaluateAutomaton() throws IOException, ClassNotFoundException {
		int textLength = (Integer)fConnection.readData();
		
		for(int i = 0; i < textLength; i++){
			fCompProcessor.loopUpdate();
			fCompProcessor.epsUpdate();
			readResult(fCompProcessor.fPaillier);
		}
	}
	
	@Override
	public void endEvaluation() throws IOException {
		fConnection.closeAllStream();
	}


	protected void readResult(Paillier paillier) throws IOException, ClassNotFoundException{
		BigInteger result = (BigInteger)fConnection.readData();
		BigInteger plainResult = paillier.dec(result);
		if(plainResult.equals(BigInteger.ZERO)){
			System.out.println("find! value: " + plainResult);
		}else{
			System.out.println("fail. value: " + plainResult);
		}
	}
}
