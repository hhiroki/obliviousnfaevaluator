package ONEcomp;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;

import base.AHMultiplier;
import base.ConnectionManager;


import jp.ac.tsukuba.mdl.hiroki.cryptgraphi.Key;
import jp.ac.tsukuba.mdl.hiroki.cryptgraphi.Paillier;


public class ONEcompAHProcessor {
	protected Key fKey;
	protected Paillier fPaillier;
	protected ArrayList<Integer> fSigma;
	protected int fAutomatonSize;
	protected AHMultiplier fMultiplier;
	
	public void setfSigma(ArrayList<Integer> fSigma) {
		this.fSigma = fSigma;
	}
	
	public Key getfKey() {
		return fKey;
	}

	public Paillier getfPaillier() {
		return fPaillier;
	}
	
	public ONEcompAHProcessor(int fAutomatonSize, int keylen, ConnectionManager connection) {
		super();
		this.fAutomatonSize = fAutomatonSize;
		fKey = new Key(keylen);
		fKey.makePaillierKey();
		fPaillier = new Paillier(fKey);
		fMultiplier = new AHMultiplier(fPaillier, fAutomatonSize, connection);
	}
	
	public BigInteger[][] makeEncMask(NFAreader nfa){
		Mask maskbit = new Mask(fKey.getN());
		maskbit.setfPlainM(maskbit.makeMask(fSigma, nfa.getfPattern(), fAutomatonSize));
		//maskbit.print(fSigma);
		return maskbit.encryptMask();
	}
	
	public BigInteger[][] makeEncLoopMask(NFAreader nfa){
		LoopMask loopMask = new LoopMask(fSigma, fAutomatonSize);
		loopMask.makeLoopMask(nfa.getfLoopInfo());
		//loopMask.printLoopMask();
		loopMask.encryptLoopMask(fPaillier);
		return loopMask.getfEncryptedLoopMask();
	}
	
	public BigInteger[][] makeEncEpsMask(NFAreader nfa){
		BigInteger[][] encEpsMask = new BigInteger[fAutomatonSize][fAutomatonSize];
		for(int to = 0; to < fAutomatonSize; to++){
			for(int from = 0; from < fAutomatonSize; from++){
				if(nfa.getfEpsInfo()[to][from] == 1){
					encEpsMask[to][from] = fPaillier.enc(0);
				}else{
					encEpsMask[to][from] = fPaillier.enc(1);	
				}
			}
		}
		printEpsMask(encEpsMask);
		return encEpsMask;
	}
	
	//for debug
	private void printEpsMask(BigInteger[][] encEpsMask){
		for(int i = 0; i < fAutomatonSize; i++){
			for(int j = 0; j < fAutomatonSize; j++){
				System.out.print(fPaillier.dec(encEpsMask[i][j]) + " ");
			}
			System.out.println();
		}
	}
	
	public void loopUpdate() throws IOException, ClassNotFoundException{
		fMultiplier.multi();
	}
	
	public void epsUpdate() throws IOException, ClassNotFoundException{
		for(int to = 0; to < fAutomatonSize; to++){
			for(int from = 0; from < fAutomatonSize; from++){
				fMultiplier.multiSingle();
				fMultiplier.multiSingle();
			}
		}
	}
}
