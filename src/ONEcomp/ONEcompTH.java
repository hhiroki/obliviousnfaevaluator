package ONEcomp;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;

import jp.ac.tsukuba.mdl.hiroki.cryptgraphi.Paillier;

import Output.OutputData;
import Output.Timer;
import base.*;

public class ONEcompTH implements TextHolderEvaluable{
	protected String fText;
	protected ConnectionManager fConnection;
	protected State fCurrentState;
	ONEcompTHProcessor fCompProcessor;
	protected THMultiplier multiplier;
	protected OutputData fOutputData;
	
	public ONEcompTH(String fText, int fPort, OutputData outputData) throws IOException {
		this.fText = fText;
		this.fConnection = new ConnectionManager(fPort);
		this.fOutputData = outputData;
	}

	@Override
	public void setupTH(int sigmaSize) throws IOException, ClassNotFoundException {
		int automatonSize = (Integer)fConnection.readData();
		ArrayList<Integer> clientSigma = (ArrayList<Integer>)fConnection.readData();
		BigInteger pk = (BigInteger)fConnection.readData();
		int msgBitLen = (Integer)fConnection.readData();
		fCompProcessor = new ONEcompTHProcessor(automatonSize, pk, msgBitLen, fConnection, fOutputData);
		

		ArrayList<Integer> mergedSigma = fCompProcessor.makeSigma(clientSigma, fText, sigmaSize);
		fCompProcessor.setfIDText(fCompProcessor.makeTextIntoID(fText));
		fConnection.writeData(mergedSigma);
		
		BigInteger[][] encMask = (BigInteger[][])fConnection.readData();
		BigInteger[][] encLoopMask = (BigInteger[][])fConnection.readData();
		BigInteger[][] encEpsMask = (BigInteger[][])fConnection.readData();
		fCompProcessor.setMasks(encMask, encLoopMask, encEpsMask);
	
		fCurrentState = new State(automatonSize, pk);
		fCurrentState.initStates();
	}
	
	@Override
	public OutputData evaluateAutomaton(int param) throws IOException,
			ClassNotFoundException {
		fConnection.writeData(fText.length());
		
		for(int i = 0; i < fText.length(); i++){
			State followingState;
			followingState = fCompProcessor.charTransUpdate(fCurrentState, i);
			followingState.setfStates(fCompProcessor.loopUpdate(followingState, fCurrentState, i));
			followingState.setfStates(fCompProcessor.epsUpdate(followingState, i));
			fCurrentState.setfStates(followingState.getfStates());
			fCompProcessor.getfOutput().endRound(i);
			sendResult(followingState);
		}
		fCompProcessor.getfOutput().makeResult();
		return fCompProcessor.getfOutput();
	}

	@Override
	public void endEvaluation() throws IOException {
		fConnection.closeAllStream();
	}
	
	
	protected void sendResult(State current) throws IOException {
		fConnection.writeData(current.getLastState());
	}
}
