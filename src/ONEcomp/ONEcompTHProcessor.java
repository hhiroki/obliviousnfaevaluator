package ONEcomp;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;

import Output.OutputData;
import base.ConnectionManager;
import base.THMultiplier;

import com.sun.org.apache.bcel.internal.generic.BIPUSH;


import jp.ac.tsukuba.mdl.hiroki.cryptgraphi.Key;
import jp.ac.tsukuba.mdl.hiroki.cryptgraphi.Paillier;

public class ONEcompTHProcessor {
	protected BigInteger fPK;
	protected int fAutomatonSize;
	protected Paillier fPaillier;
	protected ArrayList<Integer> fSigma;
	protected int[] fIDText;
	protected THMultiplier fMultiplier;
	protected BigInteger[][] fEncMask;
	protected BigInteger[][] fEncLoopMask;
	protected BigInteger[][] fEncEpsMask;
	protected OutputData fOutput;

	public void setMasks(BigInteger[][] encMask, BigInteger[][] encLoopMask, BigInteger[][] encEpsMask){
		fEncMask = encMask;
		fEncLoopMask = encLoopMask;
		fEncEpsMask = encEpsMask;
	}
	
	public void setfIDText(int[] fIDText) {
		this.fIDText = fIDText;
	}

	
	public OutputData getfOutput() {
		return fOutput;
	}

	public ONEcompTHProcessor(int automataSize, BigInteger pk, int msgSpaceSize, ConnectionManager connection, OutputData output){
		fAutomatonSize = automataSize;
		fPK = pk;
		fPaillier = new Paillier(fPK);
		fMultiplier = new THMultiplier(fPaillier, fAutomatonSize, connection, msgSpaceSize);
		fOutput = output;
	}
	
	public ArrayList<Integer> makeSigma(ArrayList<Integer> clientSigma, String text, int sigmaSize){
		char[] textChar = text.toCharArray();
		for(char c: textChar){
			int charValue = (int) c;
			if(clientSigma.contains(charValue) == false){
				clientSigma.add(charValue);
			}
		}
		
		//inflate sigma 
		if(sigmaSize != -1){
			int sigma = 0;
			while(clientSigma.size() < sigmaSize){
				if(clientSigma.contains(sigma) == false){
					clientSigma.add(sigma);
				}
				sigma++;
			}
		}
		fSigma = clientSigma;
		return fSigma;
	}
	
	public int[] makeTextIntoID(String text){
		char[] textChar = text.toCharArray();
		int[] id = new int[textChar.length];
		for(int i = 0; i < id.length; i++){
			for(int j = 0;j < fSigma.size(); j++){
				if(fSigma.get(j) == (int)textChar[i]){
					id[i] = j;
				}
			}
		}
		return id;
	}
	
	public State charTransUpdate(State previousState, int index){
		State followingState = new State(fAutomatonSize, fPK);
		followingState.setfStates(followingState.charTransUpdate(fEncMask, previousState, fIDText[index], fOutput));
		return followingState;
	}
	
	public BigInteger[] loopUpdate(State followingState, State previousState, int index) throws IOException, ClassNotFoundException{
		BigInteger[] temp = previousState.plus(fEncLoopMask[fIDText[index]]);
		BigInteger[] updated = fMultiplier.multi(followingState.getfStates(), temp, fOutput);
		return updated;
	}
	
	public BigInteger[] epsUpdate(State followingState, int index) throws IOException, ClassNotFoundException{
		BigInteger temp1;
		BigInteger[] updated = new BigInteger[fAutomatonSize];
		BigInteger initNum = fPaillier.enc(1);
		
		for(int to = 0; to < fAutomatonSize; to++){
			BigInteger prod = initNum;
			BigInteger[] added = followingState.plus(fEncEpsMask[to]);
			for(int from = 0; from < fAutomatonSize; from++){
				temp1 = fMultiplier.multiSingle(added[from], followingState.getfStates()[to], fOutput);
				prod = fMultiplier.multiSingle(prod, temp1, fOutput);
			}
			updated[to] = prod;
		}
		return updated;
	}	
}
