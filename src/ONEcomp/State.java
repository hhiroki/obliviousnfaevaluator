package ONEcomp;

import java.math.BigInteger;

import Output.OutputData;

import jp.ac.tsukuba.mdl.hiroki.cryptgraphi.Paillier;

public class State {
	BigInteger[] fStates;
	BigInteger fPK;
	Paillier fPaillier;
	
	public State(int automataSize, BigInteger pk) {
		super();
		fStates = new BigInteger[automataSize];
		fPK = pk;
		fPaillier = new Paillier(pk);
	}

	public BigInteger getfState(int i) {
		return fStates[i];
	}
	
	public BigInteger[] getfStates() {
		return fStates;
	}
	
	public BigInteger getLastState(){
		return fStates[fStates.length-1];
	}
	
	public void setfStates(BigInteger[] fStates) {
		this.fStates = fStates;
	}
	
	public void initStates(){
		fStates[0] = fPaillier.enc(0);
		for(int i = 1; i < fStates.length; i++){
			fStates[i] = fPaillier.enc(1);
		}
	}
	
	public BigInteger[] charTransUpdate(BigInteger[][] encM, State beforeS, int textID, OutputData output){
		BigInteger[] newS = new BigInteger[fStates.length];
		newS[0] = fPaillier.enc(BigInteger.ZERO);
		output.getThTimer().start();
		for(int j = 1; j < fStates.length; j++){
			newS[j] = fPaillier.addCipher(beforeS.getfStates()[j-1], encM[textID][j-1]); 
		}
		output.getThTimer().stop();
		return newS;
	}	

	public BigInteger[] plus(BigInteger[] targetValue){
		BigInteger[] tempBI = new BigInteger[fStates.length];
		for(int i = 0; i < tempBI.length; i++){
			tempBI[i] = fPaillier.addCipher(fStates[i], targetValue[i]);
		}
		return tempBI;
	}
}