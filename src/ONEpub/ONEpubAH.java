package ONEpub;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;

import jp.ac.tsukuba.mdl.hiroki.cryptgraphi.Paillier;


import base.AutomatonHolderEvaluable;
import ONEcomp.NFAreader;
import ONEcomp.ONEcompAH;

public class ONEpubAH extends ONEcompAH implements AutomatonHolderEvaluable{
	ONEpubAHProcessor fPubProcessor;
	
	public ONEpubAH(int fKeylen, int fPort, InetAddress fIp)
			throws IOException {
		super(fKeylen, fPort, fIp);
	}

	@Override
	public void setupAH(File nfaFile) throws IOException,
			ClassNotFoundException {
		// TODO Auto-generated method stub
		super.fNFA = new NFAreader(nfaFile);
		super.fNFA.readNFA();
		fPubProcessor = new ONEpubAHProcessor(fNFA.getfAutomatonLen(), fKeylen, fConnection);
		
		super.fConnection.writeData(fNFA.getfAutomatonLen());
		super.fConnection.writeData(fNFA.getfSigma());
		super.fConnection.writeData(fPubProcessor.getfKey().getN());
		super.fConnection.writeData(fKeylen*2); //encrypted message space.
		
		fPubProcessor.setfSigma((ArrayList<Integer>)super.fConnection.readData());
		
		super.fConnection.writeData(fPubProcessor.makeEncMask(fNFA));
		super.fConnection.writeData(fPubProcessor.makeEncLoopMask(fNFA));
		super.fConnection.writeData(fPubProcessor.makeLoopInfo(fNFA));
		super.fConnection.writeData(fPubProcessor.makeEpsInfo(fNFA));
	}


	@Override
	public void evaluateAutomaton() throws IOException, ClassNotFoundException {
		int textLength = (Integer)fConnection.readData();
		
		for(int i = 0; i < textLength; i++){
			fPubProcessor.loopUpdate();
			fPubProcessor.epsUpdate();
			readResult(fPubProcessor.getfPaillier());
		}
	}

	@Override
	public void endEvaluation() throws IOException {
		// TODO Auto-generated method stub
		super.endEvaluation();
	}

	@Override
	protected void readResult(Paillier paillier) throws IOException,
			ClassNotFoundException {
		// TODO Auto-generated method stub
		super.readResult(paillier);
	}



	
}
