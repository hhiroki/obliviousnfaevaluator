package ONEpub;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;

import base.ConnectionManager;

import ONEcomp.LoopMask;
import ONEcomp.NFAreader;
import ONEcomp.ONEcompAHProcessor;

public class ONEpubAHProcessor extends ONEcompAHProcessor{
	int[] fLoopInfo;
	int[][] fEpsInfo;
	public ONEpubAHProcessor(int fAutomatonSize, int keylen,
			ConnectionManager connection) {
		super(fAutomatonSize, keylen, connection);
	}
	
	@Override
	public BigInteger[][] makeEncMask(NFAreader nfa) {
		return super.makeEncMask(nfa);
	}

	@Override
	public BigInteger[][] makeEncLoopMask(NFAreader nfa) {
		return super.makeEncLoopMask(nfa);
	}

	public int[] makeLoopInfo(NFAreader nfa){
		fLoopInfo = new int[super.fAutomatonSize];
		for(int i = 0; i < super.fAutomatonSize; i++){
			ArrayList<Integer> loopArray = nfa.getfLoopInfo().get(i);
			if(loopArray.isEmpty() == true){
				fLoopInfo[i] = 0;
			}else{
				fLoopInfo[i] = 1;
			}
		}
		return fLoopInfo;
	}
	
	public int[][] makeEpsInfo(NFAreader nfa){
		fEpsInfo = nfa.getfEpsInfo();
		return fEpsInfo;
	}
	
	@Override
	public void loopUpdate() throws IOException, ClassNotFoundException {
		for(int i = 0; i < super.fAutomatonSize; i++){
			if(fLoopInfo[i] == 1){
				super.fMultiplier.multiSingle();
			}
		}
	}

	@Override
	public void epsUpdate() throws IOException, ClassNotFoundException {
		for(int to = 0; to < super.fAutomatonSize; to++){
			for(int from = 0; from < super.fAutomatonSize; from++){
				if(fEpsInfo[to][from] == 1){
					super.fMultiplier.multiSingle();
				}
			}
		}
	}
	
}
