package ONEpub;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;



import ONEcomp.ONEcompTH;
import ONEcomp.ONEcompTHProcessor;
import ONEcomp.State;
import Output.OutputData;

public class ONEpubTH extends ONEcompTH{
	ONEpubTHProcessor fPubProcessor;
	
	public ONEpubTH(String fText, int fPort, OutputData output) throws IOException {
		super(fText, fPort, output);
	}

	@Override
	public void setupTH(int sigmaSize) throws IOException,
			ClassNotFoundException {
		int automatonSize = (Integer)super.fConnection.readData();
		ArrayList<Integer> clientSigma = (ArrayList<Integer>)super.fConnection.readData();
		BigInteger pk = (BigInteger)super.fConnection.readData();
		int msgSpaceSize = (Integer)super.fConnection.readData();
		fPubProcessor = new ONEpubTHProcessor(automatonSize, pk, msgSpaceSize, super.fConnection, super.fOutputData);
		
		ArrayList<Integer> mergedSigma = fPubProcessor.makeSigma(clientSigma, super.fText, sigmaSize);
		fPubProcessor.setfIDText(fPubProcessor.makeTextIntoID(super.fText));
		super.fConnection.writeData(mergedSigma);
		
		BigInteger[][] encMask = (BigInteger[][])super.fConnection.readData();
		BigInteger[][] encLoopMask = (BigInteger[][])super.fConnection.readData();
		int[] loopInfos = (int[])super.fConnection.readData();
		int[][] epsInfos = (int[][])super.fConnection.readData();
		fPubProcessor.setMasks(encMask, encLoopMask, loopInfos, epsInfos);
	
		super.fCurrentState = new State(automatonSize, pk);
		super.fCurrentState.initStates();

	}

	@Override
	public OutputData evaluateAutomaton(int param) throws IOException,
			ClassNotFoundException {
		super.fConnection.writeData(fText.length());
		
		for(int i = 0; i < super.fText.length(); i++){
			State followingState;
			followingState = fPubProcessor.charTransUpdate(fCurrentState, i);
			followingState.setfStates(fPubProcessor.loopUpdate(followingState, fCurrentState, i));
			followingState.setfStates(fPubProcessor.epsUpdate(followingState, i));
			fCurrentState.setfStates(followingState.getfStates());
			fPubProcessor.getfOutput().endRound(i);
			super.sendResult(followingState);
		}
		fPubProcessor.getfOutput().makeResult();
		return fPubProcessor.getfOutput();
	}
	
	
	
}
