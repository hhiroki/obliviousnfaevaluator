package ONEpub;

import java.io.IOException;
import java.math.BigInteger;

import base.ConnectionManager;


import ONEcomp.ONEcompTHProcessor;
import ONEcomp.State;
import Output.OutputData;

public class ONEpubTHProcessor extends ONEcompTHProcessor{
	int[] fLoopInfos;
	int[][] fEpsInfos;
	
	public ONEpubTHProcessor(int automataSize, BigInteger pk, int msgSpaceSize,
			ConnectionManager connection, OutputData output) {
		super(automataSize, pk, msgSpaceSize, connection, output);
		// TODO Auto-generated constructor stub
	}

	public void setMasks(BigInteger[][] encMask, BigInteger[][] encLoopMask, int[] loopInfos, int[][] epsInfos) {
		super.fEncMask = encMask;
		super.fEncLoopMask = encLoopMask;
		fLoopInfos = loopInfos;
		fEpsInfos = epsInfos;
	}

	@Override
	public BigInteger[] loopUpdate(State followingState, State previousState,
			int index) throws IOException, ClassNotFoundException {
		BigInteger[] updated = new BigInteger[super.fAutomatonSize];
		for(int i = 0; i < super.fAutomatonSize; i++){
			if(fLoopInfos[i] == 1){
				BigInteger temp = super.fPaillier.addCipher(previousState.getfState(i), super.fEncLoopMask[super.fIDText[index]][i]);
				updated[i] = super.fMultiplier.multiSingle(followingState.getfState(i), temp, super.fOutput);
			}else{
				updated[i] = followingState.getfState(i);
			}
		}
		return updated;
	}

	@Override
	public BigInteger[] epsUpdate(State followingState, int index)
			throws IOException, ClassNotFoundException {
		BigInteger[] updated = new BigInteger[super.fAutomatonSize];
		for(int to = 0; to < super.fAutomatonSize; to++){
			updated[to] = followingState.getfState(to);
			for(int from = 0; from < fAutomatonSize; from++){
				if(fEpsInfos[to][from] == 1){
					updated[to] = fMultiplier.multiSingle(followingState.getfState(to), followingState.getfState(from), super.fOutput);
				}
			}
		}
		return updated;
	}
}
