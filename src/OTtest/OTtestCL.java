package OTtest;

import java.io.IOException;
import java.math.BigInteger;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;

import Output.OutputData;
import Output.Timer;
import base.AHObliviousTransfer;
import base.ConnectionManager;

import sun.net.InetAddressCachePolicy;

public class OTtestCL {
	public static void main(String[] args) throws IOException, ClassNotFoundException{
		InetAddress fIp = Inet4Address.getByName("192.168.100.20");
		ConnectionManager fconnection = new ConnectionManager(2345, fIp);
		int keylen = 64;
		BigInteger[] value = new BigInteger[3];
		value[0] = BigInteger.ZERO;
		value[1] = BigInteger.ONE;
		value[2] = BigInteger.TEN;

		AHObliviousTransfer ot = new AHObliviousTransfer(keylen, fconnection);
		Timer tempTimer = new Timer(1);
		ot.phQuery(value, tempTimer);

	}
}

