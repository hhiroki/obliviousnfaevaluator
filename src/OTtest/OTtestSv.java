package OTtest;

import java.io.IOException;

import Output.OutputData;
import base.ConnectionManager;
import base.THObliviousTransfer;

public class OTtestSv {
	public static void main(String[] args) throws IOException, ClassNotFoundException{
		ConnectionManager fConnection = new ConnectionManager(2345);
		THObliviousTransfer ot = new THObliviousTransfer(fConnection);
		int index = 2;
		OutputData tempOutput = new OutputData(null, null, 1);
		System.out.println(ot.shQuery(index, tempOutput));
	}
}
