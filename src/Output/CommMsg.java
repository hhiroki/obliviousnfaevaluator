package Output;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class CommMsg {
	int fCommSize;
	int fMaxCount;
	
	public CommMsg(int maxcount) {
		super();
		this.fCommSize = 0;
		fMaxCount = maxcount;
	}

	
	public void addComm(int commsize){
		fCommSize += commsize;
	}

	public void addComm(int commsize1, int commsize2){
		fCommSize += commsize1 + commsize2;
	}
	
	public void addComm(int commsize1, int commsize2, int commsize3){
		fCommSize += commsize1 + commsize2 + commsize3;
	}
	
	public double getAverageCommSize() {
		return (double)fCommSize / (double)fMaxCount;
	}
	
	
}
