package Output;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class FileSorter {

	public FileSorter() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int sortOverwrite(File inputFile) throws IOException{
		BufferedReader reader = new BufferedReader(new FileReader(inputFile));
		ArrayList<Integer> key = new ArrayList<Integer>();
		ArrayList<Double> value = new ArrayList<Double>();
		String line;
		while((line = reader.readLine()) != null){
			String[] data = line.split(" ");
			key.add(Integer.parseInt(data[0]));
			value.add(Double.parseDouble(data[1]));
		}
		reader.close();
		if(key.size() < 2){
			return -1;
		}
		int datanumber = key.size();
		PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(inputFile)));
		ArrayList<String> sortedData = new ArrayList<String>();
		ArrayList<Integer> sortedKey = new ArrayList<Integer>();
		//1st
		sortedData.add(key.get(0) + " " + value.get(0));
		sortedKey.add(key.get(0));
		//2nd
		if(key.get(1) > key.get(0)){
			sortedData.add(key.get(1) + " " + value.get(1));
			sortedKey.add(key.get(1));
		}else{
			sortedData.add(0, key.get(1) + " " + value.get(1));
			sortedKey.add(0, key.get(1));
		}
		for(int i = 2; i < datanumber; i++){
			int tmpKey = key.get(i);
			//最小
			if(tmpKey < sortedKey.get(0)){
				sortedKey.add(0, tmpKey);
				sortedData.add(0, tmpKey + " " + value.get(i));					
			}
			//最大
			if(tmpKey > sortedKey.get(sortedKey.size() - 1)){
				sortedKey.add(tmpKey);
				sortedData.add(tmpKey + " " + value.get(i));					
			}
			//中間
			for(int j = 0; j < sortedData.size() - 1; j++){
				if(tmpKey > sortedKey.get(j) && tmpKey < sortedKey.get(j + 1)){
					sortedKey.add(j + 1, tmpKey);
					sortedData.add(j + 1, tmpKey + " " + value.get(i));					
				}
			}
		}
		
		for(String str: sortedData){
			writer.write(str+"\n");
		}
		writer.close();
		return 0;
	}
	
	public void sortAll(File dir) throws IOException{
		for(String file: dir.list()){
			sortOverwrite(new File(dir, file));
		}
	}
}
