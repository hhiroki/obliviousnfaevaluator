package Output;

public class HeapSpace {
	long space;
	
	public HeapSpace() {
		this.space = 0;
	}

	public boolean checkUpdate(){
		Runtime runtime = Runtime.getRuntime();
		System.gc();
		long tempSpace = runtime.totalMemory() - runtime.freeMemory();
		if(space < tempSpace){
			space = tempSpace;
			return true;
		}
		return false;
	}

	public long getSpace() {
		return space;
	}

	public void setSpace(long space) {
		this.space = space;
	}
	
	
}
