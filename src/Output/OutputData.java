package Output;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;


/**
 * <h1>output data format class</h1>
 */
public class OutputData {
	String[] fKeysetArray;
	String fAlgName;
	File fDir;
	double[] result;
	String[] filenames = {
			"THCompTime",
			"AHCompTime",
			"CommTime",
			"WholeCompTime",
			"wholeTime",
			"CommSize",
	};
	double[] datavalue = {
			1000,
			1000,
			1000,
			1000,
			1000,
			1024,
	};
	
	Timer thTimer;
	Timer ahTimer;
	Timer commTimer;
	CommMsg commSize;
	
	

	public Timer getThTimer() {
		return thTimer;
	}

	public Timer getAhTimer() {
		return ahTimer;
	}

	public Timer getCommTimer() {
		return commTimer;
	}

	public CommMsg getCommSize() {
		return commSize;
	}
	
	public OutputData(File dir, String algName, int textLen) {
		this.fDir = dir;
		fAlgName = algName;
		thTimer = new Timer(textLen);
		ahTimer = new Timer(textLen);
		commTimer = new Timer(textLen);
		commSize = new CommMsg(textLen);
		result = new double[filenames.length];
	}
	
	public void endRound(int index){
		thTimer.endRound(index);
		ahTimer.endRound(index);
		commTimer.endRound(index);
	}
	
	public void makeResult(){
		thTimer.compResult();
		ahTimer.compResult();
		commTimer.compResult();
		result[0] = thTimer.getfResultTime();	//thcomp
		result[1] = ahTimer.getfResultTime();	//ahcomp
		result[2] = commTimer.getfResultTime();	//commtime
		result[3] = result[0] + result[1];		//wholecomp
		result[4] = result[3] + result[2];		//wholetime
		result[5] = commSize.getAverageCommSize();//commsize
	}
	
	public void writeResult(int param) throws IOException{
		for(int i = 0; i < result.length; i++){
			File outputFile = new File(fDir, fAlgName + filenames[i] + param + ".dat");
			PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(outputFile, true)));
			writer.println(param + " " + (result[i] / datavalue[i]));
			writer.close();
		}
		FileSorter sorter = new FileSorter();
		sorter.sortAll(fDir);
	}
}
