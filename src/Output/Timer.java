package Output;
/**
 * timerクラスの使い方
 * 1.Timer(maxcount)でmaxcount回の実行をsetup
 * 2.start(), stop()間の処理の時間を計算
 * 3.endRoud(index)でindex回目の実行を終了
 * 4.show(msg)で一回の実行の時間を表示
 * 5.showResult(msg)でmaxcount回の実行の平均時間を出力
 * @author Harada
 *
 */
public class Timer {
	//fields
	long fTempTime;
	long fRunningTime;
	double fResultTime;
	long[] fResultHistory;
	int fMaxcount;
	
	//constructor
	public Timer(int maxcount) {
		this.fMaxcount = maxcount;
		this.fResultHistory = new long[maxcount];
		this.fRunningTime = 0;
	}
	//getter
	
	public double getfResultTime() {
		return fResultTime;
	}
	
	public void setfRunningTime(long fRunningTime) {
		this.fRunningTime = fRunningTime;
	}

	public long getfRunningTime() {
		return fRunningTime;
	}
	
	public void addTime(long additionaltime){
		fRunningTime = fRunningTime + additionaltime;
	}
	//methods
	public void start(){
		fTempTime = System.currentTimeMillis();
	}


	public void stop(){
		fRunningTime += System.currentTimeMillis() - fTempTime;
	}
	
	public void endRound(int count){
		fResultHistory[count] =  fRunningTime;
		fTempTime = 0;
		fRunningTime = 0;
	}
	
	public void show(String msg){
		System.out.println(msg + ": " + fRunningTime);
	}
	
	public void compResult(){
		long sum = 0;
		for(int i = 0; i < fMaxcount; i++){
			sum += fResultHistory[i];
		}
		fResultTime = sum / (double)fMaxcount;
	}
	
	public void showResult(String msg){
		System.out.print(msg + ": ");
		System.out.println(fResultTime);
	}
}