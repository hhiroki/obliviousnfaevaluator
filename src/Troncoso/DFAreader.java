package Troncoso;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class DFAreader {
	File fFile;
	int[][] fDelta;
	ArrayList<Integer> fRegSigma;
	int fAutomatonLen;
	int[] from;
	int[] value;
	int[] to;

	public DFAreader(File fFile) {
		super();
		this.fFile = fFile;
		fRegSigma = new ArrayList<Integer>();
	}

	public int[][] getfDelta() {
		return fDelta;
	}

	public ArrayList<Integer> getfSigma() {
		return fRegSigma;
	}

	public int getfAutomatonLen() {
		return fAutomatonLen;
	}

	public void readDFA()throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(fFile));
		String line;
		ArrayList<String[]> dfaList = new ArrayList<String[]>();
		while((line = reader.readLine() ) != null){
			String[] trans = line.split(",");
			dfaList.add(trans);
		}
		
		//decide pattern length
		int automatonLen = findMaxStateNum(dfaList) + 1;
		fAutomatonLen = automatonLen;
		
		//read dfa and make sigma
		from = new int[dfaList.size()];
		value = new int[dfaList.size()];
		to = new int[dfaList.size()];
		for(int i = 0; i < dfaList.size(); i++){
			from[i] = Integer.parseInt(dfaList.get(i)[0]);
			value[i] = Integer.parseInt(dfaList.get(i)[1], 16);
			to[i] = Integer.parseInt(dfaList.get(i)[2]);

			if(fRegSigma.contains(value[i]) != true){
				fRegSigma.add(value[i]);
			}
		}
	}
	
	public void makeDelta(ArrayList<Integer> sigma){
		fDelta = new int[fAutomatonLen][sigma.size()];
		for(int i = 0 ; i < fAutomatonLen;i++){
			for(int j = 0; j < sigma.size(); j++){
				fDelta[i][j] = 0;
			}
		}
		for(int i = 0; i < from.length; i++){
			fDelta[from[i]][sigma.indexOf(value[i])] = to[i];
		}
		for(int i = 0; i < sigma.size(); i++){
			fDelta[fAutomatonLen - 1][i] = fAutomatonLen - 1;
		}
	}

	private int findMaxStateNum(ArrayList<String[]> nfaList){
		int max = -1;
		for(String[] str : nfaList){
			int fromNum = Integer.parseInt(str[0]);
			int toNum = Integer.parseInt(str[2]);
			if(max < toNum){
				max = toNum;
			}
			if(max < fromNum){
				max = fromNum;
			}
		}
		return max;
	}
}
