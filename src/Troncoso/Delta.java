package Troncoso;

import java.math.BigInteger;
import java.util.ArrayList;

public class Delta {
	private int[][] fDelta;
	private int[] fFinalState;
	private ArrayList<Integer> fSigma;
	private int fAutomatonSize;
	
	public Delta(ArrayList<Integer> sigma, DFAreader dfa){
		fAutomatonSize = dfa.getfAutomatonLen();
		fSigma = sigma;
		dfa.makeDelta(sigma);
		fDelta = dfa.getfDelta();
	}
	
	public ArrayList<Integer> getfSigma() {
		return fSigma;
	}
	
	public int getAutomataLen(){
		return fAutomatonSize;
	}

	public void setfSigma(ArrayList<Integer> fSigma) {
		this.fSigma = fSigma;
	}
	
	public BigInteger[] firstRandomize(int randomizer){
		BigInteger[] v = new BigInteger[fSigma.size()];
		for(int i = 0; i < v.length; i++){
			v[i] = BigInteger.valueOf((fDelta[0][i] + randomizer)% fAutomatonSize);
		}
		return v;
	}
	
	public int[][] makeKthDelta(int ra, int raPlus){
		int[][] randomizedDelta = new int[fAutomatonSize][fSigma.size()];
		//randomize
		for(int i = 0; i < fAutomatonSize; i++){
			for(int j = 0; j < fSigma.size(); j++){
				randomizedDelta[i][j] = (fDelta[i][j] + raPlus) % fAutomatonSize;
			}
		}
		//rotate
		int[][] kThDelta = new int[fAutomatonSize][fSigma.size()];
		for(int i = 0; i < fAutomatonSize; i++){
			kThDelta[(i+ra) % fAutomatonSize] = copy(randomizedDelta[i]);
		}
		return kThDelta;
	}
		
	private int[] copy(int[] a){
		int[] b = new int[a.length];
		for(int i = 0; i < a.length; i++){
			b[i] = a[i];
		}
		return b;
	}
	
	public void showDelta(){
		System.out.print("sigma : ");
		for(int i = 0; i < fSigma.size(); i++){
			System.out.print(fSigma.get(i)+" ");
		}
		System.out.println(" ");
		for(int i = 0; i < fDelta.length; i++){
			System.out.print("i = " + i + " :");
			for(int j = 0; j < fDelta[0].length; j++){
				System.out.print(" " + fDelta[i][j]);
				if(j == fDelta[0].length-1){
					System.out.println(" ");
				}
			}
		}
	}
}
