package Troncoso;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Random;


import base.AutomatonHolderEvaluable;
import base.ConnectionManager;

public class TroncosoAH implements AutomatonHolderEvaluable {
	DFAreader fDFA;
	TroncosoAHProcessor fProcessor;
	ConnectionManager fConnection;
	
	
	
	public TroncosoAH(int fPort, InetAddress fIp) throws IOException {
		fConnection = new ConnectionManager(fPort, fIp);
	}

	@Override
	public void setupAH(File automatonFile) throws IOException,
			ClassNotFoundException {
		fDFA = new DFAreader(automatonFile);
		fDFA.readDFA();

		BigInteger pk = (BigInteger)fConnection.readData();
		fConnection.writeData(fDFA.getfAutomatonLen());
		fConnection.writeData(fDFA.getfSigma());
		ArrayList<Integer> mergedSigma = (ArrayList<Integer>)fConnection.readData();
		fProcessor = new TroncosoAHProcessor(pk, fDFA, mergedSigma, fConnection);

	}

	@Override
	public void evaluateAutomaton() throws IOException, ClassNotFoundException {
		int textSize = (Integer)fConnection.readData();
		Random rand =new Random();
		int ra = rand.nextInt(Integer.MAX_VALUE) % fProcessor.fAutomatonSize;
		int raPlus;

		for(int i = 0; i < textSize; i++){
			if(i == 0){
				fProcessor.firstUpdate(ra);
			}else{
				raPlus = rand.nextInt(Integer.MAX_VALUE) % fProcessor.fAutomatonSize;
				fProcessor.kthUpdate(ra, raPlus, fConnection);
				ra = raPlus;
			}
			readResult(ra);
		}
	}

	public void readResult(int ra)throws IOException, ClassNotFoundException{
		int rb = (Integer)fConnection.readData();
		int result = fProcessor.mod(rb - ra, fProcessor.fAutomatonSize);
		if(result == fProcessor.fAutomatonSize - 1){
			System.out.println("FIND! value: " + result);
		}else
			System.out.println("fail. value: " + result);
	} 
	
	@Override
	public void endEvaluation() throws IOException {
		fConnection.closeAllStream();
	}

}
