package Troncoso;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;

import com.sun.org.apache.bcel.internal.generic.FCONST;

import jp.ac.tsukuba.mdl.hiroki.cryptgraphi.Paillier;
import Output.Timer;
import base.AHObliviousTransfer;
import base.ConnectionManager;

public class TroncosoAHProcessor {
	AHObliviousTransfer fOT;
	Paillier fPaillier;
	ArrayList<Integer> fSigma;
	Delta fDelta;
	int fAutomatonSize;
	
	public TroncosoAHProcessor(BigInteger pk, DFAreader dfa, ArrayList<Integer> sigma, ConnectionManager connection) {
		super();
		this.fAutomatonSize = dfa.getfAutomatonLen();
		int bitlen = pk.bitLength()*2;
		fOT = new AHObliviousTransfer(bitlen, connection);
		fPaillier = new Paillier(pk);
		fSigma = sigma;
		fDelta = new Delta(fSigma, dfa);
	}
	public void setfSigma(ArrayList<Integer> fSigma) {
		this.fSigma = fSigma;
	}
	
	public void firstUpdate(int ra) throws IOException, ClassNotFoundException{
		Timer ahTimer = new Timer(1);
		ahTimer.start();
		BigInteger[] v0 = fDelta.firstRandomize(ra);
		ahTimer.stop();
		
		fOT.phQuery(v0, ahTimer);
	}
	
	public void kthUpdate(int ra, int raPlus, ConnectionManager connection)throws IOException, ClassNotFoundException{
		Timer ahTimer = new Timer(1);
		ahTimer.start();
		int[][] kthDelta = fDelta.makeKthDelta(ra, raPlus);
		ahTimer.stop();
		BigInteger[] e = (BigInteger[])connection.readData();
		connection.writeData("null"); // for compTimeCheck.
		ahTimer.start();
		BigInteger[] vk = matrixMultiply(kthDelta, e);
		ahTimer.stop();
		fOT.phQuery(vk, ahTimer);
	}
	
	private BigInteger[] matrixMultiply(int[][] kthDelta, BigInteger[] e){
		BigInteger[] v = new BigInteger[fSigma.size()];
		for(int i = 0; i < v.length; i++){
			v[i] = fPaillier.enc(0);
			for(int j = 0; j < fAutomatonSize; j++){
				BigInteger temp = fPaillier.multiplyCipher(e[j], BigInteger.valueOf(kthDelta[j][i]));
				v[i] = fPaillier.addCipher(v[i], temp);
			}
		}
		return v;
	}
	
	public int mod(int a, int m){
		if(a < 0){
			while(a < 0){
				a = a + m;
			}
		}
		return a % m;
	}
}
