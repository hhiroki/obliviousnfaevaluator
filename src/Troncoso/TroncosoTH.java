package Troncoso;

import java.io.IOException;
import java.util.ArrayList;


import Output.OutputData;
import base.ConnectionManager;
import base.TextHolderEvaluable;

public class TroncosoTH implements TextHolderEvaluable{
	String fText;
	int fKeylen;
	ConnectionManager fConnection;
	TroncosoTHProcessor fProcessor;
	OutputData fOutputData;
	
	public TroncosoTH(String text, int keylen, int port, OutputData outputData) throws IOException {
		super();
		this.fText = text;
		this.fKeylen = keylen;
		this.fConnection = new ConnectionManager(port);
		this.fOutputData = outputData;
	}

	@Override
	public void setupTH(int sigmaSize) throws IOException,
			ClassNotFoundException {
		fProcessor = new TroncosoTHProcessor(fText, fKeylen, fConnection, fOutputData);
		fConnection.writeData(fProcessor.fKey.getN());
		fProcessor.setfAutomatonSize((Integer)fConnection.readData());
		ArrayList<Integer> clientSigma = (ArrayList<Integer>)fConnection.readData();
		ArrayList<Integer> mergedSigma = fProcessor.makeSigma(clientSigma, fText, sigmaSize);
		fConnection.writeData(mergedSigma);

		fProcessor.setfIDText(fProcessor.makeTextIntoID(fText));
	}

	@Override
	public OutputData evaluateAutomaton(int param) throws IOException,
			ClassNotFoundException {
		fConnection.writeData(fText.length());
		int rb = 0;
		for(int i  = 0; i < fText.length(); i++){
			if(i == 0){
				rb = fProcessor.firstUpdate();
			}else{
				rb = fProcessor.kthUpdate(i, rb, fConnection);
			}
			sendResult(rb);
			fOutputData.endRound(i);
		}
		fOutputData.makeResult();
		return fOutputData;
	}

	public void sendResult(int rb) throws IOException{
		fConnection.writeData(rb);
	}
	
	@Override
	public void endEvaluation() throws IOException {
		// TODO Auto-generated method stub
		fConnection.closeAllStream();
	}
	
}
