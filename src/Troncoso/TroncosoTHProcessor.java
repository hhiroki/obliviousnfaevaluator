package Troncoso;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;

import base.ConnectionManager;
import base.THObliviousTransfer;
import ONEcomp.ONEcompTHProcessor;
import Output.OutputData;
import Output.Timer;
import jp.ac.tsukuba.mdl.hiroki.cryptgraphi.Key;
import jp.ac.tsukuba.mdl.hiroki.cryptgraphi.Paillier;


public class TroncosoTHProcessor{
	Key fKey;
	Paillier fPaillier;
	ArrayList<Integer> fSigma;
	int[] fIDText;
	int fAutomatonSize;
	THObliviousTransfer fOT;
	OutputData fOutputData;
	
	public TroncosoTHProcessor(String text, int keylen, ConnectionManager fConnection, OutputData output) {
		this.fKey = new Key(keylen);
		fKey.makeRSAKey();
		this.fPaillier = new Paillier(fKey);
		this.fSigma = new ArrayList<Integer>();
		fOT = new THObliviousTransfer(fConnection);
		this.fOutputData = output;
	}
	
	public OutputData getfOutput(){
		return fOutputData;
	}
	
	public void setfIDText(int[] fIDText) {
		this.fIDText = fIDText;
	}

	public void setfAutomatonSize(int fAutomatonSize) {
		this.fAutomatonSize = fAutomatonSize;
	}

	public ArrayList<Integer> makeSigma(ArrayList<Integer> clientSigma, String text, int sigmaSize){
		char[] textChar = text.toCharArray();
		for(char c: textChar){
			int charValue = (int) c;
			if(clientSigma.contains(charValue) == false){
				clientSigma.add(charValue);
			}
		}
		
		//inflate sigma 
		if(sigmaSize != -1){
			int sigma = 0;
			while(clientSigma.size() < sigmaSize){
				if(clientSigma.contains(sigma) == false){
					clientSigma.add(sigma);
				}
				sigma++;
			}
		}
		fSigma = clientSigma;
		return fSigma;
	}
	
	public int[] makeTextIntoID(String text){
		char[] textChar = text.toCharArray();
		int[] id = new int[textChar.length];
		for(int i = 0; i < id.length; i++){
			for(int j = 0;j < fSigma.size(); j++){
				if(fSigma.get(j) == textChar[i]){
					id[i] = j;
				}
			}
		}
		return id;
	}

	public int firstUpdate()throws IOException, ClassNotFoundException{
		return fOT.shQuery(fIDText[0], fOutputData).intValue();
	}
	
	public int kthUpdate(int k, int rb, ConnectionManager connection)throws IOException, ClassNotFoundException{
		fOutputData.getThTimer().start();
		BigInteger[] encE = new BigInteger[fAutomatonSize];
		for(int i = 0; i < fAutomatonSize; i++){
			if(i == rb){
				encE[i] = fPaillier.enc(1);
			}else{
				encE[i] = fPaillier.enc(0);
			}
		}
		fOutputData.getThTimer().stop();
		for(int i = 0; i < encE.length; i++){
			fOutputData.getCommSize().addComm(encE[i].bitLength());
		}
		
		Timer temp = new Timer(1);
		temp.start();
		connection.writeData(encE);
		connection.readData(); //for comTime evaluation
		temp.stop();
		fOutputData.getCommTimer().addTime(temp.getfRunningTime());
		BigInteger encRB = fOT.shQuery(fIDText[k], fOutputData);
		return fPaillier.dec(encRB).intValue();
	}
}
