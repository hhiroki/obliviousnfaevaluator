package base;

import java.io.IOException;
import java.math.BigInteger;

import Output.Timer;
import jp.ac.tsukuba.mdl.hiroki.cryptgraphi.Paillier;

public class AHMultiplier {
	protected Paillier fPaillier;
	protected int fAutomatonLen;
	protected BigInteger fEncZero;
	protected ConnectionManager fConnection;
	
	
	public ConnectionManager getfConnection() {
		return fConnection;
	}

	public AHMultiplier(Paillier fPaillier, int fAutomatonLen,
			ConnectionManager fConnection) {
		super();
		this.fPaillier = fPaillier;
		this.fAutomatonLen = fAutomatonLen;
		this.fConnection = fConnection;
		fEncZero = fPaillier.enc(0);
	}
	
	public void multiSingle()throws IOException, ClassNotFoundException{
		BigInteger primeX = (BigInteger)fConnection.readData();
		BigInteger primeY = (BigInteger)fConnection.readData();
		Timer ahCompTime = new Timer(1);
		ahCompTime.start();
		BigInteger primeZ = fPaillier.dec(primeX).multiply(fPaillier.dec(primeY));
		BigInteger encPrimeZ = fPaillier.enc(primeZ);
		ahCompTime.stop();
		fConnection.writeData(encPrimeZ);
		fConnection.writeData(ahCompTime.getfRunningTime());
	}
	
	public void multi()throws IOException, ClassNotFoundException{
		BigInteger[] primeX = new BigInteger[fAutomatonLen];
		BigInteger[] primeY = new BigInteger[fAutomatonLen];
		primeX = (BigInteger[])fConnection.readData();
		primeY = (BigInteger[])fConnection.readData();
		Timer ahCompTime = new Timer(1);
		ahCompTime.start();
		BigInteger[] primeZ = new BigInteger[fAutomatonLen];
		BigInteger[] encPrimeZ = new BigInteger[fAutomatonLen];
		primeZ  = new BigInteger[primeX.length];
		for(int i = 0; i < primeX.length; i++){
			primeZ[i] = (fPaillier.dec(primeX[i]).multiply(fPaillier.dec(primeY[i]))).mod(fPaillier.getN());
			encPrimeZ[i] = fPaillier.enc(primeZ[i]);
		}
		ahCompTime.stop();
		fConnection.writeData(encPrimeZ);
		fConnection.writeData(ahCompTime.getfRunningTime());
	}
}
