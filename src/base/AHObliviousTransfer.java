package base;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Random;

import Output.Timer;

import jp.ac.tsukuba.mdl.hiroki.cryptgraphi.Key;

public class AHObliviousTransfer {
	ConnectionManager fConnection;
	private Key fRSAKey;
	
	public AHObliviousTransfer(int keylen, ConnectionManager connection){
		fConnection = connection;
		fRSAKey = new Key(keylen);
		fRSAKey.makeRSAKey();
	}
		
	public void phQuery(BigInteger[] vector, Timer ahTimer)throws IOException, ClassNotFoundException{
		fConnection.writeData(fRSAKey.getBit());
		fConnection.writeData(fRSAKey.getE());
		fConnection.writeData(fRSAKey.getN());
		
		ahTimer.start();
		Random rand = new Random();
		BigInteger[] randomX = new BigInteger[vector.length];
		for(int i = 0; i < randomX.length; i++){
			randomX[i] = new BigInteger(fRSAKey.getBit(), rand).mod(fRSAKey.getN());
		}
		ahTimer.stop();
		fConnection.writeData(randomX);
		BigInteger v = (BigInteger)fConnection.readData();
		
		ahTimer.start();
		BigInteger[] ks = new BigInteger[vector.length];
		for(int i = 0; i < vector.length; i++){
			ks[i] = (v.subtract(randomX[i])).modPow(fRSAKey.getD(), fRSAKey.getN());
		}
		BigInteger[] addedVector = new BigInteger[vector.length];
		for(int i = 0; i < vector.length; i++){
			addedVector[i] = vector[i].add(ks[i]);
		}
		ahTimer.stop();
		fConnection.writeData("ack"); //for commTimer
		fConnection.writeData(addedVector);
		fConnection.writeData(ahTimer.getfRunningTime());
	}
}
