package base;

import java.io.File;
import java.io.IOException;

public interface AutomatonHolderEvaluable  {
	void setupAH(File automatonFile)throws IOException, ClassNotFoundException;
	void evaluateAutomaton() throws IOException, ClassNotFoundException;
	void endEvaluation() throws IOException;
}
