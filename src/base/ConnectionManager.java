package base;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

public class ConnectionManager {
	Socket fSocket;
	ServerSocket fServerSocket;
	InputStream fIs;
	OutputStream fOs;
	DataInputStream fDis;
	DataOutputStream fDos;
	ObjectInputStream fOis;
	ObjectOutputStream fOos;

	//for Automaton holder
	public ConnectionManager(int fPort, InetAddress fIp) throws IOException {
		System.out.println("Client Started, port:" + fPort);
		fSocket = new Socket(fIp, fPort);
		fIs = fSocket.getInputStream();
		fOs = fSocket.getOutputStream();
		fDis = new DataInputStream(fIs);
		fDos = new DataOutputStream(fOs);
		fOos = new ObjectOutputStream(fDos);
		fOis = new ObjectInputStream(fDis);
	}

	//for Text holder
	public ConnectionManager(int fPort) throws IOException{
		System.out.println("Server Started, port:" + fPort);
		fServerSocket = new ServerSocket(fPort);
		fSocket = fServerSocket.accept();
		System.out.println("Client connected.");
		fIs = fSocket.getInputStream();
		fOs = fSocket.getOutputStream();
		fDis = new DataInputStream(fIs);
		fDos = new DataOutputStream(fOs);
		fOos = new ObjectOutputStream(fDos);
		fOis = new ObjectInputStream(fDis);
	}
	
	public void writeData(Object obj) throws IOException{
		fOos.writeObject(new SerializedData(obj));
	}
	
	public Object readData() throws IOException, ClassNotFoundException{
		Object msg = ((SerializedData)fOis.readObject()).getfObject();
		return msg;
	}
	public void closeAllStream()throws IOException{
		fOis.close();
		fOos.close();
		fDis.close();
		fDos.close();
	}
	
}
