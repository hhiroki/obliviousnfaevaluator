package base;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.ArrayList;

public class SerializedData implements Serializable{
	Object fObject;
	
	public SerializedData(Object obj) {
		super();
		fObject = obj;
	}

	public Object getfObject() {
		return fObject;
	}
	
	
}
