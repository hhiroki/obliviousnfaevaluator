package base;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Random;

import Output.OutputData;
import Output.Timer;

import jp.ac.tsukuba.mdl.hiroki.cryptgraphi.Paillier;

public class THMultiplier {
	protected Paillier fPaillier;
	protected int fAutomatonLen;
	protected ConnectionManager fConnection;
	protected Random fRand;
	protected int fMsgBitSize;
	
	public THMultiplier(Paillier fPaillier, int fAutomatonLen,
			ConnectionManager fConnection, int msgBitSize) {
		super();
		this.fPaillier = fPaillier;
		this.fAutomatonLen = fAutomatonLen;
		this.fConnection = fConnection;
		this.fMsgBitSize = msgBitSize;
		this.fRand = new Random();
	}
	
	public BigInteger multiSingle(BigInteger x, BigInteger y, OutputData output)throws IOException, ClassNotFoundException{
		output.getThTimer().start();
		BigInteger u = new BigInteger(fMsgBitSize, fRand);
		BigInteger v = new BigInteger(fMsgBitSize, fRand);
		BigInteger primeX = fPaillier.addCipher(x, fPaillier.enc(u));
		BigInteger primeY = fPaillier.addCipher(y, fPaillier.enc(v));
		BigInteger primeZ;
		output.getThTimer().stop();
		
		Timer temp = new Timer(1);
		temp.start();
		fConnection.writeData(primeX);
		fConnection.writeData(primeY);
		primeZ = (BigInteger)fConnection.readData();
		temp.stop();
		long ahCompTime = (Long)fConnection.readData();

		output.getCommSize().addComm(primeX.bitLength());
		output.getCommSize().addComm(primeY.bitLength());
		output.getCommSize().addComm(primeZ.bitLength());
		
		output.getAhTimer().setfRunningTime(ahCompTime);
		output.getCommTimer().setfRunningTime(temp.getfRunningTime() - ahCompTime);

		output.getThTimer().start();
		primeZ = fPaillier.subtractCipher(primeZ, fPaillier.multiplyCipher(y, u));
		primeZ = fPaillier.subtractCipher(primeZ, fPaillier.multiplyCipher(x, v));
		primeZ = fPaillier.subtractCipher(primeZ, fPaillier.enc(u.multiply(v)));
		output.getThTimer().stop();
		return primeZ;	
	}

	
	public BigInteger[] multi(BigInteger[] x, BigInteger[] y, OutputData output)throws IOException, ClassNotFoundException{
		
		output.getThTimer().start();
		BigInteger[] u = makeRandomNums(fRand, fAutomatonLen);
		BigInteger[] v = makeRandomNums(fRand, fAutomatonLen);
		BigInteger[] primeX = randomize(x, u);
		BigInteger[] primeY = randomize(y, v);
		BigInteger[] primeZ = new BigInteger[primeX.length];
		output.getThTimer().stop();
		
		Timer temp = new Timer(1);
		temp.start();
		
		fConnection.writeData(primeX);
		fConnection.writeData(primeY);
		primeZ = (BigInteger[])fConnection.readData();
		
		temp.stop();
		long ahCompTime = (Long)fConnection.readData();

		for(int i  = 0; i < primeX.length; i++){
			output.getCommSize().addComm(primeX[i].bitLength());
			output.getCommSize().addComm(primeY[i].bitLength());
			output.getCommSize().addComm(primeZ[i].bitLength());
		}
		output.getAhTimer().setfRunningTime(ahCompTime);
		output.getCommTimer().setfRunningTime(temp.getfRunningTime() - ahCompTime);
		
		output.getThTimer().start();
		for(int i = 0; i < primeZ.length; i++){
			primeZ[i] = fPaillier.subtractCipher(primeZ[i], fPaillier.multiplyCipher(y[i], u[i]));
			primeZ[i] = fPaillier.subtractCipher(primeZ[i], fPaillier.multiplyCipher(x[i], v[i]));
			primeZ[i] = fPaillier.subtractCipher(primeZ[i], fPaillier.enc(u[i].multiply(v[i])));
		}
		output.getThTimer().stop();
		
		return primeZ;
	}
		
	protected BigInteger[] makeRandomNums(Random rand, int dim){
		BigInteger[] r = new BigInteger[dim];
		for(int i = 0; i < dim; i++){
			r[i] = new BigInteger(fMsgBitSize, fRand);
		}
		return r;
	}
	
	
	protected BigInteger[] randomize(BigInteger[] x, BigInteger[] u){
		BigInteger[] primeX = new BigInteger[x.length];
		for(int i = 0; i < x.length; i++){
			primeX[i] = fPaillier.addCipher(x[i], fPaillier.enc(u[i]));
		}
		return primeX;
	}
}
