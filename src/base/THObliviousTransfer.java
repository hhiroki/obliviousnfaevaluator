package base;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Random;

import Output.OutputData;
import Output.Timer;


import jp.ac.tsukuba.mdl.hiroki.cryptgraphi.Key;


public class THObliviousTransfer {
	private ConnectionManager fConnection;
	
	public THObliviousTransfer(ConnectionManager connection) {
		super();
		fConnection = connection;
	}

	public BigInteger shQuery(int index, OutputData output)throws IOException, ClassNotFoundException{		
		int bitLen = (Integer)fConnection.readData();
		BigInteger keyE = (BigInteger)fConnection.readData();
		BigInteger keyN = (BigInteger)fConnection.readData();

		output.getCommTimer().start();
		//alice
		BigInteger[] randomX = (BigInteger[])fConnection.readData();
		output.getCommTimer().stop();
		for(int i = 0; i < randomX.length; i++){
			output.getCommSize().addComm(randomX[i].bitLength());
		}

		//Bob
		output.getThTimer().start();
		Random rand = new Random();
		BigInteger randomK = new BigInteger(bitLen, rand);
		
		BigInteger temp1 = randomK.modPow(keyE, keyN);
		BigInteger temp2 = randomX[index].mod(keyN);
		BigInteger v = (temp1.add(temp2)).mod(keyN);
		output.getThTimer().stop();
		output.getCommSize().addComm(v.bitLength());
		output.getCommTimer().start();
		fConnection.writeData(v);
		output.getCommTimer().stop();
		//Alice
		fConnection.readData(); //for commTimer
		output.getCommTimer().start();
		BigInteger[] message = (BigInteger[])fConnection.readData();
		output.getCommTimer().stop();
		for(int i = 0; i < message.length; i++){
			output.getCommSize().addComm(message[i].bitLength());
		}
		//Bob
		output.getThTimer().start();
		BigInteger mes = message[index].subtract(randomK);
		output.getThTimer().stop();
		output.getCommTimer().stop();
		
		output.getAhTimer().setfRunningTime((Long)fConnection.readData());
		return mes;
	}
}
