package base;

import java.io.IOException;

import Output.OutputData;


public interface TextHolderEvaluable  {
	
	void setupTH(int sigmaSize) throws IOException, ClassNotFoundException;
	OutputData evaluateAutomaton(int param) throws IOException, ClassNotFoundException;
	void endEvaluation() throws IOException;
	
}
