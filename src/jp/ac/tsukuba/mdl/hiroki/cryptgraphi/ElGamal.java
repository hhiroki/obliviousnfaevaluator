package jp.ac.tsukuba.mdl.hiroki.cryptgraphi;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;

public class ElGamal {


	/**
	 * <p><code>ElGamal</code>クラスは平文をElGamal暗号で暗号化，復号するクラスです．</p>
	 * @author harada hiroki
	 * <hr />
	 * 
	 * <h3>簡単な使い方</h3>
	 * <pre>
	 * Key key = new Key(512);	//p,g,x,yのbit設定
	 * key.makeElGamalKey();	//鍵生成
	 * ElGamal elgamal;
	 * elgamal = new ElGamal(key);	//暗号・復号関数作成
	 * elgamal.enc(BigInteger);		//暗号化
	 * elgamal.dec(BigInteger);		//復号
	 * </pre>
	 * 
	 * <h3>ElGamal 暗号の基本（脳みそに叩きこめ！）</h3>
	 * <dl>
	 * <dt>a,b	</dt><dd>暗号文		</dd>
	 * <dt>m	</dt><dd>平文		</dd>
	 * <dt>p	</dt><dd>大きな素数	</dd>
	 * <dt>n	</dt><dd>平文空間		p*q</dd>
	 * <dt>n^2	</dt><dd>暗号文空間	</dd>
	 * <dt>g	</dt><dd>生成元		g = n+1 (mod n^2)	...... 正確にはg = nk+1 (mod n^2)</dd>
	 * <dt>暗号化関数</dt><dd>Ea(m) = g^k mod p, Eb(m) = y^kM mod p	</dd>
	 * <dt>復号関数</dt><dd>M = b/a^x mod p</dd>
	 * </dl>
	 * 
	 * <h3>改訂履歴</h3>
	 * <table class="zebra-striped">
	 * <thead><tr><th class="blue header">Version	</th><th class="blue header">内容	</th><th class="blue header">日付		</th><th class="blue header">名前			</th><th class="blue header">備考		</th></tr></thead>
	 * <tbody>
	 * <tr><td>1.0		</td><td>Create	</td><td>2013/07/03	</td><td>Harada		</td><td>			</td></tr>
	 * </tbody>
	 * </table> 
	 * 
	 */
		
		private BigInteger g;	//g < p
		private BigInteger p;	//Prime number
		private BigInteger y;	//y < p
		private BigInteger x;	//x < p
		private int bit;
		private SecureRandom rnd;
		
		/**
		 * 公開鍵nを入力としたElGamalクラスのコンストラクタ．<br/>
		 * 暗号化オブジェクトとして利用するときはこっち．
		 * @param n 公開鍵n
		 */
		public ElGamal(BigInteger n){
			// set Publickey
			this.rnd = new SecureRandom();
		}
		
		
		/**
		 * 入力を公開鍵n，秘密鍵lとしたPaillierクラスのコンストラクタ．<br/>
		 * 暗号化/復号オブジェクトと利用する場合はこっち．
		 * @param n 公開鍵n
		 * @param l 秘密鍵l
		 *
		public Paillier(BigInteger n, BigInteger l){
			// set Publickey
			this.n = n;
			this.n2 = n.pow(2);
			this.g = n.add(BigInteger.ONE);
			this.rnd = new SecureRandom();
			// set Secretkey
			this.l = l;
			this.ll = Paillier.L(this.g.modPow(this.l, this.n2), this.n).modInverse(this.n);
		}
		/
		
		/**
		 * 	Keyオブジェクトを入力としたPaillierクラスのコンストラクタ．
		 * @param inputKey Keyオブジェクト
		 */
		public ElGamal(Key inputKey){
			// set PublicKey
			this.p = inputKey.getP();
			//計算時間比較テスト
//			this.p = this.p.pow(2);
			this.g = inputKey.getG();
			this.y = inputKey.getY();
			this.rnd = new SecureRandom();
			this.bit = inputKey.getBit();

			// set SecretKey
			this.x = inputKey.getX();
		}
		
		
		
		/**
		 * Paillier暗号のL関数． L(u,n) = (u-1)/n (mod n)
		 * @param u 入力u
		 * @param n mod n
		 * @return (u-1)/n (mod n)
		 */
		public static BigInteger L(BigInteger u, BigInteger n){
			return u.subtract(BigInteger.ONE).divide(n).mod(n);
		}
		
		/**
//		 * Paillierオブジェクトの持つkeyを一覧表示する関数．
//		 */
//		public void displayPublicKey(){
//			System.out.println("[Key] n      : " + this.n );
//			System.out.println("[Key] n^2    : " + this.n2);
//			System.out.println("[Key] g      : " + this.g );
//		}
//		public void displaySecretKey(){
//			System.out.println("[Key] p      : " + this.p );
//			System.out.println("[Key] q      : " + this.q );
//			System.out.println("[Key] labmda : " + this.l );
//		}
//		

		

		/**
		 * 入力をBigInteger型の平文とし，BigInteger型の暗号文を返す．
		 * @param mess 平文
		 * @return 暗号文
		 */
		public BigInteger[] enc(int i){
			return enc(BigInteger.valueOf(i));
		}
		
		public BigInteger[] enc(BigInteger mess){
			// r must be taken by multiplicative group of n.
			BigInteger k = new BigInteger(this.bit, this.rnd);
			do {
				k = new BigInteger(this.bit, rnd);
			} while (! k.gcd(this.p.subtract(BigInteger.ONE)).equals(BigInteger.ONE));
			k = k.mod(p);
			BigInteger[] c = new BigInteger[2];
			c[0] = this.g.modPow(k, this.p);
			//c[1] = this.y.modPow(k, this.p).mod(p);
			c[1] = this.y.modPow(k, this.p).multiply(mess).mod(p);
			return c;
		}
		

		/**
		 * 暗号文を入力として復号結果を返す．
		 * @param c 暗号文
		 * @return 復号結果
		 * @throws ArithmeticException
		 */
		// c^l mod n2 を CRT出とくと速い？pqpq.
		
		public BigInteger dec(BigInteger[] c) throws ArithmeticException{
			return c[1].multiply(c[0].modInverse(p).modPow(this.x, this.p)).mod(p);
		}
		
		
		/**
		 * 平文と，平文空間nを入力として，符号付きのmを返す．
		 * @param m 平文
		 * @param n 平文空間
		 * @return 符号付きの平文
		 */
		public static BigInteger getSign(BigInteger m, BigInteger n){
			if(m.compareTo(n.divide(new BigInteger("2"))) >= 0){
				return n.multiply(new BigInteger("-1")).add(m);
			}else{
				return m;
			}
		}
		
		
		/**
		 * 2つの暗号文E(m1)とE(m2)を入力して，乗算結果の暗号文E(m1*m2)を返す．
		 * @param c1[2] 暗号文
		 * @param c2[2] 暗号文
		 * @return 平文の乗算の暗号文
		 */
		public BigInteger[] multiplyCipher(BigInteger[] c1, BigInteger[] c2){
			BigInteger[] ans = new BigInteger[2];
			ans[0] = c1[0].multiply(c2[0]).mod(p);
			ans[1] = c1[1].multiply(c2[1]).mod(p);
			return ans;
		}
		public static String getString(BigInteger m){
//			return new String(m.toByteArray(), Charset.forName("UTF-8"));
			return new String(m.toByteArray());
		}
		
		
		
		
		/**
		 * args[0]:bit of key length, args[1]:Message
		 * @param args 
		 */
		public static void main(final String[] args){
			int KEY_LENGTH = 3;
			Key key = new Key(KEY_LENGTH);
			key.makeElGamalKey();
			ElGamal elGamal = new ElGamal(key);
			System.out.println("Key length : " + KEY_LENGTH);

			BigInteger[] tmpCipher1 = new BigInteger[2];
			BigInteger[] tmpCipher2 = new BigInteger[2];
			BigInteger[] tmpCipher3 = new BigInteger[2];

			System.out.println("乗算テスト");
			tmpCipher1 = elGamal.enc(BigInteger.valueOf(1));	
			System.out.println(elGamal.dec(tmpCipher1));
			tmpCipher2 = elGamal.enc(BigInteger.valueOf(2));
			System.out.println(elGamal.dec(tmpCipher2));
			for(int i=1; i<=2; i++){
				tmpCipher3 = elGamal.multiplyCipher(tmpCipher1, tmpCipher2);
				System.out.println("Dec:\t"+ elGamal.dec(tmpCipher3));
			}
//			
//			System.out.println("符号判別");
//			BigInteger tmpPlain, tmpDec;
//			for (int i = 0; i < ROUND; i++) {
//				tmpPlain = new BigInteger(String.valueOf(rnd.nextInt(100000000)-(100000000/2)));
//				tmpCipher = paillier.enc(tmpPlain);
//				tmpDec = paillier.dec(tmpCipher);
////				System.out.println(tmpPlain + "\t" + tmpDec + "\t" + Paillier.getSign(tmpDec, paillier.n));
//				System.out.println(tmpPlain + "\t" + Paillier.getSign(tmpDec, paillier.n));
//			}

		}
}
