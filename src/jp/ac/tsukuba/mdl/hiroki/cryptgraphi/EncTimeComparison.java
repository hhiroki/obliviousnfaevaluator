package jp.ac.tsukuba.mdl.hiroki.cryptgraphi;

import java.math.BigInteger;
import java.util.Random;

import Output.Timer;



public class EncTimeComparison {
	public static void main(String[] args){
		int keylength = Integer.parseInt(args[0]);
		int trycount = Integer.parseInt(args[1]);
		Key key = new Key(keylength);
		key.makePaillierKey();
		Paillier paillier = new Paillier(key);
		key.makeElGamalKey();
		ElGamal elgamal = new ElGamal(key);
		Random rand = new Random();
		
		//paillier�Í���
		Timer paiEncTimer = new Timer(trycount);
		BigInteger[] bi = new BigInteger[trycount];
		for(int i = 0; i < trycount; i++){
			paiEncTimer.start();
			bi[i] = paillier.enc(rand.nextInt());
			paiEncTimer.stop();
			paiEncTimer.endRound(i);		
			}
		paiEncTimer.compResult();
		paiEncTimer.showResult("paillier encryption");
		
		Timer paiDecTimer = new Timer(trycount);
		for(int i = 0; i < trycount; i++){
			paiDecTimer.start();
			paillier.dec(bi[i]);
			paiDecTimer.stop();
			paiDecTimer.endRound(i);		
			}
		paiDecTimer.compResult();
		paiDecTimer.showResult("paillier deryption");		
		
		
		Timer egTimer = new Timer(trycount);
		for(int i = 0; i < trycount; i++){
			int t1 = rand.nextInt(100);
			//System.out.print("plain:" + t1);
			//System.out.println(", dec: " + elgamal.dec(elgamal.enc(t1)));
			egTimer.start();
				elgamal.enc(rand.nextInt());
			egTimer.stop();
			egTimer.endRound(i);
		}
		egTimer.compResult();
		egTimer.showResult("ElGamal encryption");
		
		

		Timer paiCompTimer = new Timer(trycount);
		for(int i = 0; i <trycount; i++){
			BigInteger e1 = paillier.enc(rand.nextInt(100));
			BigInteger e2 = paillier.enc(rand.nextInt(100));
			paiCompTimer.start();
			for(int j = 0; j < 100; j++){
				paillier.addCipher(e1, e2);
			}
			paiCompTimer.stop();
			paiCompTimer.endRound(i);
		}
		paiCompTimer.compResult();
		paiCompTimer.showResult("Paillier encadd");

		Timer EGCompTimer1 = new Timer(trycount);
		for(int i = 0; i <trycount; i++){
			BigInteger[] e1 = elgamal.enc(rand.nextInt());
			BigInteger[] e2 = elgamal.enc(rand.nextInt());
			EGCompTimer1.start();
			for(int j = 0; j < 100; j++){
				elgamal.multiplyCipher(e1, e2);
			}
			EGCompTimer1.stop();
			EGCompTimer1.endRound(i);
		}
		EGCompTimer1.compResult();
		EGCompTimer1.showResult("EGComp");

	}
}
