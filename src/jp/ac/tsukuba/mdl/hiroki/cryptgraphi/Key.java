package jp.ac.tsukuba.mdl.hiroki.cryptgraphi;

import java.math.BigInteger;
import java.util.Random;


/**
 * <p><code>Key</code>クラスはPaillier/RSA暗号専用鍵ジェネレータで，
 * Pailleir暗号，RSA暗号用の公開鍵と秘密鍵のペアを生成する．
 * </p>
 * @author Nakazato
 * @author Yoshiki Aoki
 * <hr />
 * 
 * <h3>簡単な使い方</h3>
 * <pre>
 * Key key = new Key(512);
 * key.makePaillierKey(); //512bitのp,q，1024bitのnを作成</pre>
 * 
 * <h3>課題一覧</h3>
 * <ul>
 *	<li>計算結果がnを超える場合の対処．</li>
 *	<li>平文mがnを超える場合の対処</li>
 *	<li>c^lとかg^lをCRTでできる？</li>
 *	<li>素数を作るところだけ継承させるとかそういう方がいい？それとかインターフェースに</li>
 * </ul>
 * 
 * <h3>改訂履歴</h3>
 * <table class="zebra-striped">
 * <thead><tr><th class="blue header">Version	</th><th class="blue header">内容	</th><th class="blue header">日付		</th><th class="blue header">名前			</th><th class="blue header">備考		</th></tr></thead>
 * <tbody>
 * <tr><td>1.0		</td><td>Create	</td><td>昔			</td><td>Nakazato		</td><td>ソースを貰い受けました（Aoki）	</td></tr>
 * <tr><td>1.1		</td><td>Update	</td><td>2010/11/16	</td><td>Yoshiki Aoki	</td><td>修論用に変更	</td></tr>
 * <tr><td>1.2		</td><td>Update	</td><td>2011/12/05	</td><td>Yoshiki Aoki	</td><td>getLCM関数を追加</td></tr>
 * <tr><td>2.0		</td><td>Update	</td><td>2012/01/31	</td><td>Yoshiki Aoki	</td><td>makePrimeを修正（ちゃんと指定bit数の素数を生成するように変更）new BigInteger(bit, 1000, rnd)から BigInteger.probablePrime(bit, rnd)へ．</td></tr>
 * <tr><td>3.0		</td><td>Update	</td><td>2012/01/31	</td><td>Harada Hiroki	</td><td>ElGamal暗号の鍵生成を追加</td></tr>
 * 
 * </tbody>
 * </table>
 */
public class Key{
	
	private Random rnd;
	private int bit;		// 鍵のbit長
	private BigInteger p;	// Prime number
	private BigInteger q;	// Prime number
	private BigInteger n;	// p*q
	private BigInteger l;	// Lamda = LCM((p-1)(q-1))
	private BigInteger d;	// n^(-1) mod l
	private BigInteger g;	// g < p
	private BigInteger y;	// g^x mod p
	private BigInteger x;	// x < p
	private BigInteger e;   // RSAの公開鍵
	
	/**
	 * 空のコンストラクタ
	 */
	public Key(){
	}

	/**
	 * 入力をp,qのbit数
	 * @param bit p,qのbit数．nではない．
	 */
	public Key(int bit){
		this.bit = bit;
		this.rnd = new Random();
	}
	
	
	/**
	 * ユークリッド互除法をつかってxとyのLCMを計算する．
	 * @param x 値
	 * @param y 値
	 * @return xとyのLCM
	 */
	public static BigInteger getLCM(BigInteger x, BigInteger y){
		BigInteger lcm = x.multiply(y);
		BigInteger gcd;
		
		do{
			if(x.compareTo(y) > 0)	x = x.mod(y);
			else					y = y.mod(x);
		}while(!( x.compareTo(BigInteger.ZERO)==0 || y.compareTo(BigInteger.ZERO)==0 ));
		
		if(x.compareTo(y) > 0)	gcd = x;
		else					gcd = y;
		return lcm.divide(gcd);	// lcm /= gcd
	}

	/**
	 * RSA用の鍵を作る．
	 * <dl>
	 * <dt>p,q	</dt><dd>大きな素数</dd>
	 * <dt>n	</dt><dd>p*q</dd>
	 * <dt>l	</dt><dd>公開鍵(p-1)(q-1)</dd>
	 * <dt>d	</dt><dd>秘密鍵n^{-1} (mod l)</dd>
	 * </dl>
	 */
	public void makeRSAKey(){
		this.p = Key.makePrime(this.rnd, this.bit);
		this.q = Key.makePrime(this.rnd, this.bit);
		this.n = this.p.multiply(this.q);
		this.l = this.p.subtract(BigInteger.ONE).multiply(this.q.subtract(BigInteger.ONE));
		this.e = BigInteger.valueOf(65537);
		this.d = this.e.modInverse(this.l);
	}
	
	/**
	 * Paillier用の鍵を作る．
	 * <dl>
	 * <dt>p,q	</dt><dd>大きな素数</dd>
	 * <dt>n	</dt><dd>p*q</dd>
	 * <dt>l	</dt><dd>LCM(p-1,q-1)</dd>
	 * </dl>
	 */
	public void makePaillierKey(){
		this.p = Key.makePrime(this.rnd, this.bit);
		this.q = Key.makePrime(this.rnd, this.bit);
		this.n = this.p.multiply(this.q);
		this.l = Key.getLCM(this.p.subtract(BigInteger.ONE), this.q.subtract(BigInteger.ONE));
	}
	
	/**
	 * ElGamal用の鍵を作る.
	 * <dl>
	 * <dt>p	</dt><dd>大きな素数</dd>
	 * <dt>g	</dt><dd>g < p</dd>
	 * <dt>x	</dt><dd> < p </dd>
	 * <dt>y	</dt><dd> g^x mod p</dd>
	 * </dl>
	 */
	public void makeElGamalKey(){
		do{
		this.p = Key.makePrime(this.rnd, this.bit);
		this.g = Key.makePrime(this.rnd, this.bit);
		if(this.g.compareTo(this.p)  >= 0){
			this.g = this.g.subtract(this.p).mod(this.p);
		}
		
		this.x = BigInteger.probablePrime(this.bit, this.rnd);
		if(this.x.compareTo(this.p) >= 0){
			this.x = this.x.subtract(this.p);
		}
		this.y = g.modPow(this.x, this.p);
		}while(p.multiply(g).multiply(x).multiply(y) == BigInteger.ZERO);
		System.out.println("g:");
		System.out.println(this.g);
		System.out.println("p:");
		System.out.println(this.p);
		System.out.println("x:");
		System.out.println(this.x);
		System.out.println("y:");
		System.out.println(this.y);
		
	}

	
	/**
	 * 指定されたbitの素数を生成する．
	 * @param rnd
	 * @param bit 素数のbit数
	 * @return bitビットの素数．
	 */
	public static BigInteger makePrime(Random rnd, int bit){
		BigInteger prime;
		do{
			prime = BigInteger.probablePrime(bit, rnd);
		}while(!prime.isProbablePrime(1000));
		return prime;
	}
	
	
	/**
	 * 公開鍵nを得る．
	 * @return 公開鍵n
	 */
	public BigInteger getN(){
		return n;
	}
	
	/**
	 * 秘密鍵pを得る．
	 * @return 秘密鍵p
	 */
	public BigInteger getP(){
		return p;
	}
	
	
	
	public BigInteger getE() {
		return e;
	}

	public BigInteger getD() {
		return d;
	}

	public BigInteger getG() {
		return g;
	}

	public BigInteger getY() {
		return y;
	}

	public BigInteger getX() {
		return x;
	}


	public int getBit() {
		return bit;
	}

	public void setBit(int bit) {
		this.bit = bit;
	}

	/**
	 * 秘密鍵qを得る．
	 * @return 秘密鍵q
	 */
	public BigInteger getQ(){
		return q;
	}
	
	/**
	 * 秘密鍵Lambdaを得る．
	 * @return 秘密鍵Lambda
	 */
	public BigInteger getLambda(){
		return l;
	}

	/**
	 * RSAの秘密鍵p,q,dを得る．
	 * @return 秘密鍵の配列
	 */
	public BigInteger[] getRSASecret(){
		BigInteger[] skey = {p, q, d};
		return skey;
	}
	
	/**
	 * Paillierの秘密鍵p,q,lを得る．
	 * @return 秘密鍵の配列
	 */
	public BigInteger[] getPaillierSecret(){
		BigInteger[] skey = {p, q, l};
		return skey;
	}
	
	
	
	//テスト用main関数
	public static void main(String[] argv){
		if(argv.length != 1){
			System.out.println("Usage : java MkKey bit");
			System.out.println("\tbit : RSA key bit length (q & p)");
		}else{
			int bit = Integer.parseInt(argv[0]);
			Key key = new Key(bit);
			key.makeRSAKey();
			BigInteger pkey = key.getN();
			BigInteger[] skey = key.getRSASecret();
			BigInteger l = key.getLambda();

			System.out.println("**************************");	
			System.out.println("* Secret Key information *");
			System.out.println("**************************");
			System.out.println("p = " + skey[0].toString(16));
			System.out.println("q = " + skey[1].toString(16));
			System.out.println("d = " + skey[2].toString(16));
			System.out.println("d = " + l.toString(16));
			System.out.println();
			System.out.println("**************************");
			System.out.println("* Public Key Information *");
			System.out.println("**************************");
			System.out.println("n = " + pkey.toString(16));
		}
	}
}
