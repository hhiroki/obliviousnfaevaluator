package jp.ac.tsukuba.mdl.hiroki.cryptgraphi;
/*
 * This is Paillier algorithm.
 * version 1.1
 * 2009.12.01 by nakazato
 *
 *
 *Update	2010/11/16	Yoshiki Aoki
*/

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;


/**
 * <p><code>Paillier</code>クラスは平文をPaillier暗号で暗号化，復号するクラスです．</p>
 * @author Nakazato
 * @author Yoshiki Aoki
 * @author Tadanori TERUYA <tadanori@mdl.cs.tsukuba.ac.jp> (2012)
 * <hr />
 * 
 * <h3>簡単な使い方</h3>
 * <pre>
 * Key key = new Key(512);	//p,qのbit設定
 * key.makePaillierKey();	//鍵生成
 * Paillier paillier;
 * paolloer = new Paillier(key);	//暗号・復号関数作成
 * paillier.enc(BigInteger);		//暗号化
 * paillier.dec(BigInteger);		//復号
 * </pre>
 * 
 * <h3>課題一覧（課題を潰していくのだ！）</h3>
 * <ul>
 *	<li>計算結果がnを超える場合の対処．</li>
 *	<li>平文mがnを超える場合の対処</li>
 *	<li>c^lとかg^lをCRTでできる？</li>
 * </ul>
 * 
 * <h3>Paillier 暗号の基本（脳みそに叩きこめ！）</h3>
 * <dl>
 * <dt>c	</dt><dd>暗号文		</dd>
 * <dt>m	</dt><dd>平文		</dd>
 * <dt>p,q	</dt><dd>大きな素数	</dd>
 * <dt>n	</dt><dd>平文空間		p*q</dd>
 * <dt>n^2	</dt><dd>暗号文空間	</dd>
 * <dt>g	</dt><dd>生成元		g = n+1 (mod n^2)	...... 正確にはg = nk+1 (mod n^2)</dd>
 * <dt>l	</dt><dd>lambda		l=LCM(p-1, q-1)</dd>
 * <dt>L関数	</dt><dd>L(u)=(u-1)/n (mod n)	</dd>
 * <dt>暗号化関数</dt><dd>E(m) = g^m r^n (mod n^2)	</dd>
 * <dt>復号関数</dt><dd>D(c) = L(c^l)/L(g^l) (mod n)</dd>
 * </dl>
 * 
 * <h3>改訂履歴</h3>
 * <table class="zebra-striped">
 * <thead><tr><th class="blue header">Version	</th><th class="blue header">内容	</th><th class="blue header">日付		</th><th class="blue header">名前			</th><th class="blue header">備考		</th></tr></thead>
 * <tbody>
 * <tr><td>1.0		</td><td>Create	</td><td>2009/12/01	</td><td>Nakazato		</td><td>			</td></tr>
 * <tr><td>1.1		</td><td>Update	</td><td>2010/11/16	</td><td>Yoshiki Aoki	</td><td>修論用に変更	</td></tr>
 * <tr><td>2.0		</td><td>Update	</td><td>2011/12/05	</td><td>Yoshiki Aoki	</td><td><ul>
 * 							<li>Paillier暗号の復号方式を変更．</li>
 *							<li>Paillier暗号の復号時に利用するL関数を追加	L(u)=(u-1)/n	...... このネーミングダメ？</li>
 *							<li>復号をL(c^l)/L(g^l)に変更．高速化．</li>
 *							<li>加算関数追加	addCipher</li>
 *							<li>減算関数追加	subtractCipher</li>
 *							<li>乗算関数追加	multiplyCipher</li>
 *							<li>符号探索関数追加	getSign</li>
 *							<li>文字列変換関数追加	getString</li>
 *							<li>その他細かいところ修正，privateつけたり，</li>
 *							<ul></td></tr>
 * <tr><td>2.1		</td><td>Update	</td><td>2012/03/22	</td><td>Yoshiki Aoki	</td><td>getStringからUTF-8の記述を削除．Java1.5ではダメらしい．	</td></tr>
 * </tbody>
 * </table> 
 * 
 */
public class Paillier{
	
	private BigInteger n;	//p*q
	private BigInteger n2;	//n^2
	private BigInteger g;	//n+1
	private BigInteger p;	//Prime number
	private BigInteger q;	//Prime number
	private BigInteger l;	//lambda LCM(p-1, q-1)
	private BigInteger ll;	//L(g^l)^{-1}
	private SecureRandom rnd;
	
	/**
	 * 公開鍵nを入力としたPaillierクラスのコンストラクタ．<br/>
	 * 暗号化オブジェクトとして利用するときはこっち．
	 * @param n 公開鍵n
	 */
	public Paillier(BigInteger n){
		// set Publickey
		this.n = n;
		this.n2 = n.pow(2);
		this.g = n.add(BigInteger.ONE);
		this.rnd = new SecureRandom();
	}
	
	
	/**
	 * 入力を公開鍵n，秘密鍵lとしたPaillierクラスのコンストラクタ．<br/>
	 * 暗号化/復号オブジェクトと利用する場合はこっち．
	 * @param n 公開鍵n
	 * @param l 秘密鍵l
	 */
	public Paillier(BigInteger n, BigInteger l){
		// set Publickey
		this.n = n;
		this.n2 = n.pow(2);
		this.g = n.add(BigInteger.ONE);
		this.rnd = new SecureRandom();
		// set Secretkey
		this.l = l;
		this.ll = Paillier.L(this.g.modPow(this.l, this.n2), this.n).modInverse(this.n);
	}
	
	
	/**
	 * 	Keyオブジェクトを入力としたPaillierクラスのコンストラクタ．
	 * @param inputKey Keyオブジェクト
	 */
	public Paillier(Key inputKey){
		// set PublicKey
		this.n = inputKey.getN();
		this.n2 = this.n.pow(2);
		this.g = this.n.add(BigInteger.ONE);
		this.rnd = new SecureRandom();
		
		// set SecretKey
		this.p = inputKey.getP();
		this.q = inputKey.getQ();
		this.l = inputKey.getLambda();	// lambda = LCM(p-1, q-1)
		this.ll = Paillier.L(this.g.modPow(this.l, this.n2), this.n).modInverse(this.n);				// L(g^l)^{-1} = (g^l-1)/n)^{-1}
	}
	
	
	
	/**
	 * Paillier暗号のL関数． L(u,n) = (u-1)/n (mod n)
	 * @param u 入力u
	 * @param n mod n
	 * @return (u-1)/n (mod n)
	 */
	public static BigInteger L(BigInteger u, BigInteger n){
		return u.subtract(BigInteger.ONE).divide(n).mod(n);
	}
	
	/**
	 * Paillierオブジェクトの持つkeyを一覧表示する関数．
	 */
	public void displayPublicKey(){
		System.out.println("[Key] n      : " + this.n );
		System.out.println("[Key] n^2    : " + this.n2);
		System.out.println("[Key] g      : " + this.g );
	}
	public void displaySecretKey(){
		System.out.println("[Key] p      : " + this.p );
		System.out.println("[Key] q      : " + this.q );
		System.out.println("[Key] labmda : " + this.l );
	}
	
	/**
	 * 入力をString形式の平文とし，BigInteger型の暗号文を返す．
	 * @param mess 平文
	 * @return 暗号文
	 */
	public BigInteger enc(String mess){
		//BigInteger m = new BigInteger(mess.getBytes());
		//return this.enc(m);
		return this.enc(new BigInteger(mess.getBytes()));
	}
	
	/**
	 * 入力をint型の平文とし，BigInteger型の暗号文を返す．
	 * @param mess 平文
	 * @return 暗号文
	 */
	public BigInteger enc(int mess){
		return this.enc(new BigInteger(String.valueOf(mess)));
	}
	
	/**
	 * 入力をBigInteger型の平文とし，BigInteger型の暗号文を返す．
	 * @param mess 平文
	 * @return 暗号文
	 */
	public BigInteger enc(BigInteger mess){
		// r must be taken by multiplicative group of n.
		BigInteger r;
		do {
			r = new BigInteger(this.n.bitLength(), rnd);
		} while (! r.gcd(this.n).equals(BigInteger.ONE));
		r = r.mod(this.n);
		BigInteger c = (BigInteger.ONE.add(this.n)).modPow(mess, this.n2).multiply(r.modPow(this.n, this.n2)).mod(this.n2);
		return c;
	}
	

	/**
	 * 暗号文を入力として復号結果を返す．
	 * @param c 暗号文
	 * @return 復号結果
	 * @throws ArithmeticException
	 */
	// c^l mod n2 を CRT出とくと速い？pqpq.
	public BigInteger dec(BigInteger c) throws ArithmeticException{
		return Paillier.dec(c, this.l, this.n2, this.n, this.ll);
	}
	public static BigInteger dec(BigInteger c, BigInteger l, BigInteger n2, BigInteger n, BigInteger ll) throws ArithmeticException{
		return Paillier.L(c.modPow(l, n2), n).multiply(ll).mod(n);
	}
	
	
	/**
	 * 平文と，平文空間nを入力として，符号付きのmを返す．
	 * @param m 平文
	 * @param n 平文空間
	 * @return 符号付きの平文
	 */
	public static BigInteger getSign(BigInteger m, BigInteger n){
		if(m.compareTo(n.divide(new BigInteger("2"))) >= 0){
			return n.multiply(new BigInteger("-1")).add(m);
		}else{
			return m;
		}
	}
	
	
	
	/**
	 * 2つの暗号文E(m1).E(m2)を入力として，2つの暗号文の平文の加算結果の暗号文E(m1+m2)を返す．
	 * @param c1
	 * @param c2
	 * @return 平文の和の暗号文．
	 */
	public BigInteger addCipher(BigInteger c1, BigInteger c2){
		return c1.multiply(c2).mod(this.n2);
	}
	
	
	/**
	 * 2つの暗号文E(m1).E(m2)を入力として，2つの暗号文の減算結果の暗号文E(m1-m2)を返す．
	 * @param c1
	 * @param c2
	 * @return 平文の差の暗号文．
	 */
	public BigInteger subtractCipher(BigInteger c1, BigInteger c2){
		return c1.multiply(c2.modInverse(this.n2)).mod(this.n2);
	}
	
	/**
	 * 1つの暗号文E(m1)と，1つの平文m2を入力として，乗算結果の暗号文E(m1*m2)を返す．
	 * @param c 暗号文
	 * @param m 平文
	 * @return 平文の乗算の暗号文
	 */
	public BigInteger multiplyCipher(BigInteger c, BigInteger m){
		return c.modPow(m, this.n2);
	}
	public static String getString(BigInteger m){
//		return new String(m.toByteArray(), Charset.forName("UTF-8"));
		return new String(m.toByteArray());
	}
	
	
	
	//ゲッターたち
	public BigInteger getN(){
		return this.n; 
	}
	public BigInteger getN2(){
		return this.n2; 
	}
	public BigInteger getG(){
		return this.g; 
	}
	public BigInteger getP(){
		return this.p;
	}
	public BigInteger getQ(){
		return this.q; 
	}
	public BigInteger getLambda(){
		return this.l; 
	}
	public BigInteger getLLambda(){
		return this.ll; 
	}
	
	
	/**
	 * args[0]:bit of key length, args[1]:Message
	 * @param args 
	 */
	public static void main(final String[] args){
//		int SIZE = 100;
		int ROUND = 30;
		int KEY_LENGTH = 512;
		long start, stop, diff, ave = 0L;
		
		Key key = new Key(KEY_LENGTH);
		start = System.nanoTime();
		key.makePaillierKey();
		Paillier paillier = new Paillier(key);
		stop = System.nanoTime();
		diff = stop-start;
		System.out.println("鍵生成：" + (double)diff/1000000 + "[ms]");
		
		start = System.nanoTime();
		Key.getLCM(paillier.p.subtract(BigInteger.ONE), paillier.q.subtract(BigInteger.ONE));
		stop = System.nanoTime();
		diff = stop-start;
		System.out.println("LCM計算: " + (double)diff/1000000 + "[ms]");
		
		System.out.println("+------------------------------------------------------------+");
		System.out.println("|  Supler Paillier                                           |");
		System.out.println("+------------------------------------------------------------+");
		
		System.out.println("Number of Round : " + ROUND);
		System.out.println("Key length : " + KEY_LENGTH);
//		paillier.displayPublicKey();
//		paillier.displaySecretKey();
		
		System.out.println("+-[Test1]----------------------------------------------------+");
		System.out.println("|  Create Cipher Text  E(x)                                  |");
		System.out.println("+------------------------------------------------------------+");
		
		//Key key = new Key(Integer.parseInt(args[0]));


		BigInteger[] plainText = new BigInteger[ROUND];
		BigInteger[] cipherText = new BigInteger[ROUND];
		BigInteger[] decText = new BigInteger[ROUND];
		Random rnd = new Random();
		
		
		for(int i=0; i<plainText.length; i++){
			plainText[i] = new BigInteger(paillier.n.bitLength()-1, rnd);
			if(plainText[i].compareTo(paillier.n) > 0){
				System.out.print(i + ",");
			}
		}
		System.out.println();
		
		System.out.println("メッセージ空間:" + paillier.n.bitLength() + " bits");
		System.out.println("暗号文空間:" + paillier.n2.bitLength() + " bits");
		
		System.out.println("暗号化E(･)にかかる計算時間を計測します．");
		
		for(int i=0; i<plainText.length; i++){
			start = System.nanoTime();
			cipherText[i] = paillier.enc(plainText[i]);
			stop = System.nanoTime();
			diff = stop - start;
//			System.out.println("\t " + (diff/1000000) + "[ms]");
			ave += diff;
		}
		ave /= plainText.length;
		System.out.println((double)ave/1000000 + "[ms]");
		
		
		System.out.println("復号D(･)にかかる計算時間を計測します．");
		ave=0L;
		for(int i=0; i<cipherText.length; i++){
			start = System.nanoTime();
			decText[i] = paillier.dec(cipherText[i]);
			stop = System.nanoTime();
			diff = stop - start;
//			System.out.print("\t " + diff + ",");
			ave += diff;
		}
		ave /= cipherText.length;
		System.out.println((double)ave/1000000 + "[ms]");
		
		
		System.out.println("乗算c*c'にかかる計算時間を計測します．");
		ave=0L;
		for(int i=0; i<cipherText.length; i++){
			start = System.nanoTime();
			cipherText[i].multiply(cipherText[cipherText.length-1-i]).mod(paillier.n2);
			stop = System.nanoTime();
			diff = stop - start;
//			System.out.print("\t " + diff + ",");
			ave += diff;
		}
		ave /= cipherText.length;
		System.out.println((double)ave/1000000 + "[ms]");
		
		
//		System.out.println("べき乗c*mにかかる計算時間を計測します．");
//		ave=0L;
//		BigInteger exp = new BigInteger("2");
		
//		for (int j=2; j<=1024 ; j*=2) {
//			exp = (new BigInteger("2")).pow(j).subtract(BigInteger.ONE);
//			System.out.print(exp + " の数字をべき乗 : ");
//			for(int i=0; i<cipherText.length; i++){
//				start = System.nanoTime();
//				cipherText[i].modPow(exp, paillier.n2);
//				stop = System.nanoTime();
//				diff = stop - start;
////				System.out.print("\t " + diff + ",");
//				ave += diff;
//			}
//			ave /= cipherText.length;
//			System.out.println((double)ave/1000000 + "[ms]");
//		}

		
		System.out.println("文字列暗号化テスト．");
		String[] testStr = {"aaas","2011/12/05","Yoshiki Aoki","Nikutabetai"};
		BigInteger tmpCipher;
		for(int i=0; i<testStr.length; i++){
			System.out.println("Plain:\t"+ testStr[i]);
			tmpCipher = paillier.enc(testStr[i]);
			System.out.println("Cipher:\t"+ tmpCipher);
			System.out.println("Dec:\t"+ (new String(paillier.dec(tmpCipher).toByteArray())) + "\t\t" + Paillier.getString(paillier.dec(tmpCipher)));
		}
		
		
		
		System.out.println("加算テスト");
		tmpCipher = paillier.enc(BigInteger.ZERO);
		for(int i=1; i<=10; i++){
			tmpCipher = paillier.addCipher(tmpCipher, paillier.enc(i));
			System.out.println("Dec:\t"+ paillier.dec(tmpCipher));
		}
		System.out.println("減算テスト");
		for(int i=1; i<=10; i++){
			tmpCipher = paillier.subtractCipher(tmpCipher, paillier.enc(i));
			System.out.println("Dec:\t"+ paillier.dec(tmpCipher));
		}
		
		System.out.println("乗算テスト");
		tmpCipher = paillier.enc(BigInteger.ONE);
		for(int i=1; i<=10; i++){
			tmpCipher = paillier.multiplyCipher(tmpCipher, new BigInteger(String.valueOf(i)));
			System.out.println("Dec:\t"+ paillier.dec(tmpCipher));
		}
		
		System.out.println("符号判別");
		BigInteger tmpPlain, tmpDec;
		for (int i = 0; i < ROUND; i++) {
			tmpPlain = new BigInteger(String.valueOf(rnd.nextInt(100000000)-(100000000/2)));
			tmpCipher = paillier.enc(tmpPlain);
			tmpDec = paillier.dec(tmpCipher);
//			System.out.println(tmpPlain + "\t" + tmpDec + "\t" + Paillier.getSign(tmpDec, paillier.n));
			System.out.println(tmpPlain + "\t" + Paillier.getSign(tmpDec, paillier.n));
		}

	}
}
