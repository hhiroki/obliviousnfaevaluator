package test;

import java.io.File;
import java.io.IOException;
import java.net.Inet4Address;
import java.net.InetAddress;

import com.sun.org.apache.bcel.internal.classfile.Field;

import ONE1eps.ONE1epsAH;
import ONEcomp.ONEcompAH;
import ONEcomp.ONEcompAHProcessor;
import ONEpub.ONEpubAH;
import Troncoso.TroncosoAH;

public class AHmain {
	public static void main(String[] args) throws IOException, ClassNotFoundException {
		int fKeylen = 512;
		int fPort = 2345;
		File dfa = new File(args[0]);
//		InetAddress fIp = Inet4Address.getByName("192.168.100.22");
		InetAddress fIp = Inet4Address.getLocalHost();
		TroncosoAH ah = new TroncosoAH(fPort, fIp);
		ah.setupAH(dfa);
		ah.evaluateAutomaton();
		ah.endEvaluation();
	}
}
