package test;

import java.io.File;
import java.io.IOException;


import ONE1eps.ONE1epsTH;
import ONEcomp.ONEcompTH;
import ONEpub.ONEpubTH;
import Output.OutputData;
import Troncoso.TroncosoTH;

public class THmain {
	public static void main(String[] args) throws IOException, ClassNotFoundException{
		String fText = "abaaaab";
		int fPort = 2345;
		int fKeylen = 512;
		String algName = "troncoso";
		File dir = new File(args[0]);
		OutputData output = new OutputData(dir, algName, fText.toCharArray().length);
		TroncosoTH th = new TroncosoTH(fText, fKeylen, fPort, output);
		
		th.setupTH(-1);
		int param = 1;
		th.evaluateAutomaton(param);
		th.endEvaluation();
		output.writeResult(param);
	}
}
