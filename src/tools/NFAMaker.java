package tools;
import java.util.ArrayList;


public class NFAMaker {
	String fRegExp;
	ArrayList<String> fNFAList;
	ArrayList<Character> fSigma;
	int fSigmaSize;
	
	public NFAMaker(String fRegExp, int sigmaSize) {
		super();
		this.fRegExp = fRegExp;
		this.fNFAList = new ArrayList<String>();
		fSigma = new ArrayList<Character>();
		fSigmaSize = sigmaSize;
		for(int i = 0; i < fSigmaSize; i++){
			fSigma.add((char) i);
		}
	}

	public ArrayList<String> parseReg(){
		int stCounter = 0;
		int regCounter = 0;
		
		String InitWildcard = "0," + charToHex('*') +",0";
		fNFAList.add(InitWildcard);
		
		while(regCounter < fRegExp.length()){
			//transCharArray setup
			ArrayList<Character> transCharArray = new ArrayList<Character>();
			char c = fRegExp.charAt(regCounter);
			if(c == '('){
				transCharArray.addAll(makeCharClass(regCounter));
				regCounter += transCharArray.size()*2 + 1;
			}else if(c == '*'){
				String wildcardSymbol = stCounter + "," + charToHex(c) + "," + (stCounter + 1);
				transCharArray.add(c);
				fNFAList.add(wildcardSymbol);
				regCounter += 1;
			}else{
				sigmaAdd(c);
				transCharArray.add(c);
				regCounter += 1;
			}
			
			//make
			if(regCounter > fRegExp.length() - 1){
				charTrans(stCounter, transCharArray);
			}else{
				switch (fRegExp.charAt(regCounter)) {
				case '[':
					stCounter = finRep(stCounter, regCounter, transCharArray);
					regCounter += 5;
					break;

				case '?':
					stCounter = optChar(stCounter, transCharArray);
					regCounter += 1;
					break;

				default:
					stCounter = charTrans(stCounter, transCharArray);
					break;
				}
			}
		}
		replaceWildCard();
		return fNFAList;
	}
	
	private void sigmaAdd(char c){
		if(fSigma.contains(c) == false){
			fSigma.add(c);
			fSigma.remove(0);
		}
	}

	private ArrayList<Character> makeCharClass(int regCounter){
		String partOfReg = "";
		for(int regSearch = regCounter + 1; fRegExp.charAt(regSearch) != ')'; regSearch++){
			partOfReg = partOfReg + fRegExp.charAt(regSearch);
		}
		String[] splitted = partOfReg.split("\\|");
		ArrayList<Character> ans = new ArrayList<Character>();
		for(int i = 0; i < splitted.length; i++){
			char c =splitted[i].toCharArray()[0];
			ans.add(c);
			sigmaAdd(c);
		}
		return ans;
	}
	
	private int charTrans(int stCounter, ArrayList<Character> charArray){
		for(char c : charArray){
			String ctrans = stCounter + "," + charToHex(c) + "," + (stCounter + 1);
			fNFAList.add(ctrans);
		}
		return stCounter + 1;
	}
	
	private void replaceWildCard(){
		for(int i = 0; i < fNFAList.size(); i++){
			String trans = fNFAList.get(i);
			String[] spl = trans.split(",");
			int from = Integer.parseInt(spl[0]);
			char c = (char)Integer.parseInt(spl[1], 16);
			int to = Integer.parseInt(spl[2]);
			
			if(c == '*'){
				fNFAList.remove(i);
				for(char sigmaChar : fSigma){
					if(sigmaChar != '*'){
						String ctrans = from + "," + charToHex(sigmaChar) + "," + to;					
						fNFAList.add(ctrans);
					}
				}
			}
		}
	}
	
	private int optChar(int stCounter, ArrayList<Character> charArray){
		for(char c : charArray){
			String ctrans = stCounter + "," + charToHex(c) + "," + (stCounter + 1);
			fNFAList.add(ctrans);
		}
		String epsTrans = stCounter + "," + "FF" + "," + (stCounter + 1);
		fNFAList.add(epsTrans);
		return stCounter + 1;
	}

	private int finRep(int stCounter, int regCounter, ArrayList<Character> charArray){
		String partOfReg = "";
		for(int regSearch = regCounter + 1; fRegExp.charAt(regSearch) != ']'; regSearch++){
			partOfReg = partOfReg + fRegExp.charAt(regSearch);
		}
		String[] indexOfFinStr = partOfReg.split(",");
		int[] indexOfFin = new int[indexOfFinStr.length];
		for(int i = 0; i < indexOfFin.length; i++){
			indexOfFin[i] = Integer.parseInt(indexOfFinStr[i]);
		}
		
		if(indexOfFin[0] > indexOfFin[1]){
			System.out.println("wrong regexp format: bad index in finiteRepeat.");
			System.exit(1);
		}
		
		for(int i = 0; i < indexOfFin[1]; i++){
			for(char c : charArray){
				String ctrans = (stCounter + i) + "," + charToHex(c) + "," + (stCounter + i + 1);
				fNFAList.add(ctrans);
			}
		}
		for(int i = indexOfFin[0] ; i < indexOfFin[1] ; i++){
			String epsTrans = (stCounter + i) + "," + "FF" + "," + (stCounter + i + 1);
			fNFAList.add(epsTrans);
		}
		
		return stCounter + indexOfFin[1];
	}

	private String charToHex(char c){
		return Integer.toHexString(c);
	}
}