package tools;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * 
 * @author Harada
 *HOW TO USE THIS
 *1. use below regex
 *	(a|b) : or
 *	a[1,3]: finite repeat
 * 	a?    : option char
 *  a*    : wild card
 */
public class NFAparser {
	public static void main(String[] args)throws IOException{
		int sigmaSize = Integer.parseInt(args[0]);
		File regDir = new File(args[1]);
		File nfaDir = new File(args[2]);
		String[] filenames = regDir.list();
		for(String filename : filenames){
			BufferedReader reader = new BufferedReader(new FileReader(new File(regDir,filename)));
			String reg = reader.readLine();
			NFAMaker nfaParser = new NFAMaker(reg, sigmaSize);
			ArrayList<String> output = nfaParser.parseReg();
			PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(new File(nfaDir, filename + ".nfa"))));
			for(String out: output){
				writer.println("(" + out + ")");
			}
			reader.close();
			writer.close();
		}
	}
}
