package tools;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class UkkonenMaker {
	int fK;
	int fKlen;
	ArrayList<ArrayList<Character>> fTextChar;
	int fpatlen;
	ArrayList<Character> fSigma;
	ArrayList<String> outputStrings;
	
	public UkkonenMaker(int fK, String fText, ArrayList<Character> sigma) {
		super();
		this.fK = fK;
		fKlen = fK + 1;
		fSigma = sigma;
		fTextChar = parseText(fText);
		fpatlen = fTextChar.size() + 1;
		outputStrings = new ArrayList<String>();
		outputStrings.addAll(sigmaTrans(0, 0));
	}
	

	public void makeUkkonen(){
		rightTransition();
		downTransition();
		diagonalTransitionEps();
		diagonalTransitionSigma();
	}
	
	public void writeNFAtoFile(PrintWriter writer)throws IOException{
		for(String s: outputStrings){
			writer.write("(" + s + ")\n");
		}
	}
	private void rightTransition(){
		for(int k = 0; k < fKlen; k++){
			int firstState = k*fpatlen;
			for(int t = 0; t < fpatlen - 1; t++){
				for(char c : fTextChar.get(t)){
					outputStrings.add((t + firstState) + "," + toHex(c) + "," + (t + firstState + 1));
				}
			}
		}
	}
	
	private void downTransition(){
		for(int k = 0; k < fKlen - 1; k++){
			int fromRowState = k*fpatlen;
			int toRowState = (k+1)*fpatlen;
			for(int t = 0; t < fpatlen; t++){
				outputStrings.addAll(sigmaTrans((t + fromRowState),(t + toRowState)));
			}
		}
	}
	
	private void diagonalTransitionEps(){
		for(int k = 0; k < fKlen - 1; k++){
			int fromRowState = k*fpatlen;
			int toRowState = (k+1)*fpatlen;
			for(int t = 0; t < fpatlen - 1; t++){
				outputStrings.add((t + fromRowState) + ",FF," + (t + toRowState + 1));
			}
		}
	}

	private void diagonalTransitionSigma(){
		for(int k = 0; k < fKlen - 1; k++){
			int fromRowState = k*fpatlen;
			int toRowState = (k+1)*fpatlen;
			for(int t = 0; t < fpatlen - 1; t++){
				outputStrings.addAll(sigmaTrans((t + fromRowState), (t + toRowState + 1)));
			}
		}
	}
	
	private ArrayList<String> sigmaTrans(int fromState, int toState){
		ArrayList<String> ans = new ArrayList<String>();
		for(char c: fSigma){
			ans.add(fromState + "," + toHex(c) + "," + toState);
		}
		return ans;
	}
	
	private ArrayList<ArrayList<Character>> parseText(String text){
		ArrayList<ArrayList<Character>> charLists = new ArrayList<ArrayList<Character>>();
		int regCounter = 0;
		while(regCounter < text.length()){
			//transCharArray setup
			ArrayList<Character> transCharArray = new ArrayList<Character>();
			if(text.charAt(regCounter) == '('){
				transCharArray.addAll(makeCharClass(regCounter, text));
				regCounter += transCharArray.size()*2 + 1;
			}else if(text.charAt(regCounter) == '*'){
				transCharArray.addAll(fSigma);
				regCounter += 1;
			}else{
				transCharArray.add(text.charAt(regCounter));
				regCounter += 1;
			}
			charLists.add(transCharArray);
		}
		return charLists;
	}
	
	private ArrayList<Character> makeCharClass(int regCounter, String text){
		String partOfReg = "";
		for(int regSearch = regCounter + 1; text.charAt(regSearch) != ')'; regSearch++){
			partOfReg = partOfReg + text.charAt(regSearch);
		}
		String[] splitted = partOfReg.split("\\|");
		ArrayList<Character> ans = new ArrayList<Character>();
		for(int i = 0; i < splitted.length; i++){
			ans.add(splitted[i].toCharArray()[0]);
		}
		return ans;
	}
	
	private String toHex(char c){
		return Integer.toHexString((int) c);
	}
}
