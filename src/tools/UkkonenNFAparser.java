package tools;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.FieldPosition;
import java.util.ArrayList;

public class UkkonenNFAparser {
	public static void main(String[] args) throws IOException{
		String text = args[0];
		int k = Integer.parseInt(args[1]);
		char[] sigmaChar = args[2].toCharArray();
		File outputDir = new File(args[3]);
		String filename = args[4];
		File outputFile1 = new File(outputDir, filename + ".unfa");
		
		ArrayList<Character> sigma = new ArrayList<Character>();
		for(char c : sigmaChar){
			sigma.add(c);
		}
		
		UkkonenMaker ukkonen = new UkkonenMaker(k, text, sigma);
		ukkonen.makeUkkonen();
		PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(outputFile1)));
		ukkonen.writeNFAtoFile(writer);
		writer.close();
	}
}
